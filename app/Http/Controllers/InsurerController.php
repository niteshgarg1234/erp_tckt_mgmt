<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Insurer;

class InsurerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $insurers = Insurer::getInsures();
            return view("master-data.manage-insurer", compact("insurers"));
        } catch (Exception $e) {
            return redirect()->back()->withErrors("Something went wrong");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $insurer = new Insurer();
            $insurer->name = $request->name;
            $insurer->short_name = $request->short_name;
            $insurer->address = $request->address;
            $insurer->save();
            return redirect("/manage-insurer");
        } catch (Exception $e) {
            return redirect()->back()->withErrors("Something went wrong");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $insurer = Insurer::where('id', $id)->first();
            $insurers = Insurer::getInsures();
            return view("master-data.edit-insurer", compact("insurer", "insurers"));
        } catch (Exception $e) {
            return redirect()->back()->withErrors("Something went wrong");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $insurer = Insurer::find($id);
            $insurer->name = $request->name;
            $insurer->short_name = $request->short_name;
            $insurer->address = $request->address;
            $insurer->save();
            return redirect("/manage-insurer");
        } catch (Exception $e) {
            return redirect()->back()->withErrors("Something went wrong");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $insurer = Insurer::find($id);
            $insurer->is_deleted = 1;
            $insurer->save();
            return redirect("/manage-insurer");
        } catch (Exception $e) {
            return redirect()->back()->withErrors("Something went wrong");
        }
    }
}
