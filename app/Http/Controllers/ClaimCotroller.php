<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
use App\Models\Insurer;
use App\Models\ClaimType;
use App\Models\Claim;
use App\Models\Activity;
use App\Models\Notification;

use App\Models\UserType;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\RoleRequest;
use App\Http\Requests\UpdatePassword;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\CreateCaseRequest;
use App\Http\Requests\CaseAssignedUserRequest;
use App\Http\Requests\CaseStatus;
use App\Http\Requests\FieldWork;
use App\Http\Requests\CaseReport;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ClaimCotroller extends Controller
{
    public function __construct()
    {
        $this->middleware('isBackOffice', ['only' => ['caseCreate', 'createCase', 'editCase', 'viewCase', 'updateCase', 'attachCaseReport', 'updateCaseAssigned', 'updateCaseStatus', 'deleteCase',]]);
        $this->middleware('isFieldOfficer', ['only' => ['manageCases', 'caseFieldWorkComplete', ]]);
    }
   
    public function caseCreate()
    {
        try {
            $insurers = Insurer::all();
            $claimTypes = ClaimType::all();
            return view('case.create-case', compact(['insurers', 'claimTypes']));  
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function createCase(CreateCaseRequest $request)
    {
        try {
            $user_id = Session::get('user_id');
            $user_name = Session::get('user_name');
            $user_role = Session::get('user_role');
            
            if ($user_role != 3) {
                $claim = new Claim();
                $claim->doi = date('Y-m-d',strtotime($request->doi));
                $claim->insurer = $request->insurer;
                $claim->claim_type = $request->claim_type;
                $claim->clid = ($request->insurer == 2)? $request->clid : '';
                $claim->foreign_id = $request->foreign_id;
                $claim->policy_no = $request->policy_no;
                $claim->native_reference = $request->native_reference;
                $claim->insurer_name = $request->insurer_name;
                $claim->hospital_name = $request->hospital_name;
                $claim->location = $request->location;
                $claim->claim_amount = $request->claim_amount;
                $claim->triggers = $request->triggers;
                $claim->user_id = $user_id;
                $claim->save();
            }
            
            
            /*********** Start Create Activity ***********/
                $activity = new \stdClass();
                $additional_parameter = new \stdClass();

                $activity->message = '<a href="#">'.ucfirst($user_name).'</a> added this case.';
                $activity->type = 1;
                $activity->claim_id = $claim->id;
                $activity->additional_parameter = $additional_parameter;
                $activity = $this->addActivity($activity);
            /*********** End Create Activity ***********/

            Session::flash('success-messages', ['Case Added Successfully.']);
            return redirect('case/all');  
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function manageCases()
    {
        try {
            $insurers = Insurer::all();
            $claimTypes = ClaimType::all();
            $status = ['Pending', 'Assigned', 'FW Completed', 'Report Attached', 'Invoice Pending', 'Payment Pending', 'Payment Done'];
            $user_id = Session::get('user_id');
            $user_role = Session::get('user_role');
            if ($user_role != 3) {
                /*$claims = Claim::where('claims.is_deleted', 0)
                                ->selectRaw('claims.*, IFNULL(CONCAT(users.first_name, " ", users.last_name), "--") AS assigned_user, insurers.short_name AS insurer_short_name, claim_types.tat')
                                ->leftJoin('users', 'users.id', '=', 'claims.assigned')
                                ->join('insurers', 'insurers.id', '=', 'claims.insurer')
                                ->join('claim_types', 'claim_types.id', '=', 'claims.claim_type')
                                ->get();*/
                $claims = Claim::getAll();
            } else {
                $claims = Claim::where(array('assigned' => $user_id, 'status' => 1, 'is_deleted' => 0))->get();
            }
            $users = User::where('role', 3)->get();
            return view('case.manage-cases', compact(['claims', 'users', 'status', 'insurers', 'claimTypes']));
            dd($claims);
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }

    public function filterCases(Request $request)
    {
        try {
            $user_id = Session::get('user_id');
            $user_role = Session::get('user_role');
            if($user_role != 3) {
                /**/
                $claims = Claim::where('claims.is_deleted', 0)
                                ->selectRaw('claims.*, IFNULL(CONCAT(users.first_name, " ", users.last_name), "--") AS assigned_user, insurers.short_name AS insurer_short_name, (datediff(now(),claims.created_at)+1) as tat')
                                ->leftJoin('users', 'users.id', '=', 'claims.assigned')
                                ->join('insurers', 'insurers.id', '=', 'claims.insurer')
                                ->join('claim_types', 'claim_types.id', '=', 'claims.claim_type')
                                ->whereNotIn('claims.status', [6]);
                if (isset($request->status)) {
                    $claims->where('claims.status', $request->status);
                }
                if (isset($request->insurer)) {
                    $claims->where('claims.insurer', $request->insurer);
                }
                if (isset($request->claim_type)) {
                    $claims->where('claims.claim_type', $request->claim_type);
                }
                if (isset($request->tat)) {
                    $claims->having('tat', '=', $request->tat);
                }
                if (isset($request->assigned_user)) {
                    $claims->having('assigned', '=', $request->assigned_user);
                }

                $claims = $claims->get();
            }
            
            $response = ['status' => 1, 'message' => "Claims Fetched.", 'claims' => $claims];
            return response()->json($response);
            dd($claims);
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }
    

    public function viewCase($id)
    {
       try {
            $user_id = Session::get('user_id');
            $user_role = Session::get('user_role');
            $activities = Activity::where('claim_id', $id)->get();
            if ($user_role != 3) {
                $insurers = Insurer::all();
                $claimTypes = ClaimType::all();
                $claim = Claim::where('id', $id)->first();
            }
            $color = ["green", "red", "blue", "purple", "light-green"];
            // dd($activities);
            return view('case.view-case', compact(['insurers', 'claimTypes', 'claim', 'color', 'activities']));
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }

    public function editCase($id)
    {
       try {
            $user_id = Session::get('user_id');
            $user_role = Session::get('user_role');
            if ($user_role != 3) {
                $insurers = Insurer::all();
                $claimTypes = ClaimType::all();
                $claim = Claim::where('id', $id)->first();
            }
            return view('case.edit-case', compact(['insurers', 'claimTypes', 'claim']));
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }

    public function updateCase(CreateCaseRequest $request, $id)
    {
       try {
            $user_id = Session::get('user_id');
            
            $claim = Claim::where('id', $id)->first();
            $claim->doi = date('Y-m-d',strtotime($request->doi));
            $claim->insurer = $request->insurer;
            $claim->claim_type = $request->claim_type;
            $claim->clid = ($request->insurer == 2)? $request->clid : '';
            $claim->foreign_id = $request->foreign_id;
            $claim->policy_no = $request->policy_no;
            $claim->native_reference = $request->native_reference;
            $claim->insurer_name = $request->insurer_name;
            $claim->hospital_name = $request->hospital_name;
            $claim->location = $request->location;
            $claim->claim_amount = $request->claim_amount;
            $claim->triggers = $request->triggers;
            $claim->user_id = $user_id;
            $claim->save();
            Session::flash('success-messages', ['Case Updated Successfully.']);
            return redirect('case/all');
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }

    public function updateCaseAssigned(CaseAssignedUserRequest $request) 
    {
       try {
            $user_id = Session::get('user_id');
            $user_role = Session::get('user_role');
            $user_name = Session::get('user_name');

            $claim = Claim::where('id', $request->case_id)->first();
            $claim->assigned = $request->assign_user;
            $claim->status = 1;
            $claim->save();
            
            /*********** Start Create Activity ***********/
            $field_worker = User::where('id', $request->assign_user)->first();
            $field_worker_name = $field_worker->first_name .' '.$field_worker->last_name;
            $message = '<a href="#">'.ucfirst($user_name).'</a> assigned this case to <span><a href="#" class="blue">'.ucfirst($field_worker_name).'</a></span>';
            
            
            $activity = new \stdClass();
            $additional_parameter = new \stdClass();
            if (strlen($request->message)) {
                $additional_parameter->message = htmlentities($request->message);
            }
            $additional_parameter->icp_files = array();
            $additional_parameter->report_files = array();
            $additional_parameter->bill_files = array();
            $additional_parameter->other_files = array();
            
            /*********** Start Uploading Files ***********/
            if ($request->hasfile('icp_files')) {
                $response = $this->filesUploder($request->file('icp_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->icp_files = $response["files"];
                }
            }

            if ($request->hasfile('report_files')) {
                $response = $this->filesUploder($request->file('report_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->report_files = $response["files"];
                }
            }

            if ($request->hasfile('bill_files')) {
                $response = $this->filesUploder($request->file('bill_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->bill_files = $response["files"];
                }
            }

            if ($request->hasfile('other_files')) {
                $response = $this->filesUploder($request->file('other_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->other_files = $response["files"];
                }
            }
            /*********** End Uploading Files ***********/

            $activity->message = $message;
            $activity->type = 1;
            $activity->claim_id = $request->case_id;
            $activity->additional_parameter = $additional_parameter;
            $activity = $this->addActivity($activity);
            /*********** End Create Activity ***********/

            Session::flash('success-messages', ['Case Status Successfully Updated.']);
            $response = array('status' => 1, 'message' => "Case Status Successfully Updated.", "activity" => $activity);
            return response()->json($response);
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }

    public function caseFieldWorkComplete(FieldWork $request)
    {
       try {
            $user_id = Session::get('user_id');
            $user_role = Session::get('user_role');
            $user_name = Session::get('user_name');
            
            
            if (in_array($user_role, [1,2])) {
                $claim = Claim::where(array('id' => $request->case_id, 'status' => 1))->first();
            }else {
                $claim = Claim::where(array('id' => $request->case_id, 'status' => 1, 'assigned' => $user_id))->first();
            }
            $claim->status = 2;
            $claim->fw_report = $request->fw_report;
            $claim->fw_details = $request->fw_details;
            $claim->save();


            /*********** Start Create Activity ***********/
            $report = ($request->fw_report == 1)? "Genuine" : "Not Genuine";
            $message = '<a href="#">'.ucfirst($user_name).'</a> completed field work. This claim is <span><a href="#" class="blue">'.$report.'</a></span>';

            $activity = new \stdClass();
            $additional_parameter = new \stdClass();
            if (strlen($request->message)) {
                $additional_parameter->message = htmlentities($request->message);
            }
            $additional_parameter->icp_files = array();
            $additional_parameter->report_files = array();
            $additional_parameter->bill_files = array();
            $additional_parameter->other_files = array();
            
            /*********** Start Uploading Files ***********/
            if ($request->hasfile('icp_files')) {
                $response = $this->filesUploder($request->file('icp_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->icp_files = $response["files"];
                }
            }

            if ($request->hasfile('report_files')) {
                $response = $this->filesUploder($request->file('report_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->report_files = $response["files"];
                }
            }

            if ($request->hasfile('bill_files')) {
                $response = $this->filesUploder($request->file('bill_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->bill_files = $response["files"];
                }
            }

            if ($request->hasfile('other_files')) {
                $response = $this->filesUploder($request->file('other_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->other_files = $response["files"];
                }
            }
            /*********** End Uploading Files ***********/

            $activity->message = $message;
            $activity->type = 1;
            $activity->claim_id = $request->case_id;
            $activity->additional_parameter = $additional_parameter;
            $activity = $this->addActivity($activity);
            /*********** End Create Activity ***********/

            Session::flash('success-messages', ['Case Status Successfully Updated.']);
            $response = array('status' => 1, 'message' => "Case Status Successfully Updated.", "activity" => $activity);
            return response()->json($response);
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }

    public function attachCaseReport(CaseReport $request)
    {
       try {
            $user_name = Session::get("user_name");
            $claim = Claim::where('id', $request->case_id)->first();
            $claim->status = 3;
            $claim->save();

            /*********** Start Create Activity ***********/
            $message = '<a href="#">'.ucfirst($user_name).'</a> created case <span><a href="#" class="blue">Report.</a></span>';

            $activity = new \stdClass();
            $additional_parameter = new \stdClass();
            if (strlen($request->message)) {
                $additional_parameter->message = htmlentities($request->message);
            }
            $additional_parameter->icp_files = array();
            $additional_parameter->report_files = array();
            $additional_parameter->bill_files = array();
            $additional_parameter->other_files = array();
            
            /*********** Start Uploading Files ***********/
            if ($request->hasfile('icp_files')) {
                $response = $this->filesUploder($request->file('icp_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->icp_files = $response["files"];
                }
            }

            if ($request->hasfile('report_files')) {
                $response = $this->filesUploder($request->file('report_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->report_files = $response["files"];
                }
            }

            if ($request->hasfile('bill_files')) {
                $response = $this->filesUploder($request->file('bill_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->bill_files = $response["files"];
                }
            }

            if ($request->hasfile('other_files')) {
                $response = $this->filesUploder($request->file('other_files'), $request->case_id);
                if ($response["status"] == 1) {
                    $additional_parameter->other_files = $response["files"];
                }
            }
            /*********** End Uploading Files ***********/

            $activity->message = $message;
            $activity->type = 1;
            $activity->claim_id = $request->case_id;
            $activity->additional_parameter = $additional_parameter;
            $activity = $this->addActivity($activity);
            /*********** End Create Activity ***********/

            Session::flash('success-messages', ['Case Status Successfully Updated.']);
            $response = array('status' => 1, 'message' => "Case Status Successfully Updated.", "activity" => $activity);
            return response()->json($response);
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }

    public function getFileimage($ext)
    {
        try {
            $image = "images/file.jpg";
            return $image;
        } catch (Exception $e) {
            
        }
    }

    public function updateCaseStatus(Request $request)
    {
       try {
            $user_name = Session::get("user_name");
            $claim = Claim::where('id', $request->case_id)->first();
            if (in_array(intval($request->case_status), array(4,5,6))) {
                $claim->status = $request->case_status;
                $claim->save();
                
                /*********** Start Create Activity ***********/
                    switch (intval($request->case_status)) {
                        case 4:
                            $message = '<a href="#">'.ucfirst($user_name).'</a> updated status to <span><a href="#" class="blue">Mail Sent</a></span>';
                            break;
                        
                        case 5:
                            $message = '<a href="#">'.ucfirst($user_name).'</a> updated status to <span><a href="#" class="blue">Payment Done</a></span>';
                            break;
                        
                        case 6:
                            $message = '<a href="#">'.ucfirst($user_name).'</a> <span><a href="#" class="blue">Closed</a></span> this case.';
                            break;

                        default:
                            $message = '';
                            break;
                    }

                    $activity = new \stdClass();
                    $additional_parameter = new \stdClass();
                    if (strlen($request->message)) {
                        $additional_parameter->message = htmlentities($request->message);
                    }
                    $additional_parameter->icp_files = array();
                    $additional_parameter->report_files = array();
                    $additional_parameter->bill_files = array();
                    $additional_parameter->other_files = array();
                    
                    /*********** Start Uploading Files ***********/
                        if ($request->hasfile('icp_files')) {
                            $response = $this->filesUploder($request->file('icp_files'), $request->case_id);
                            if ($response["status"] == 1) {
                                $additional_parameter->icp_files = $response["files"];
                            }
                        }

                        if ($request->hasfile('report_files')) {
                            $response = $this->filesUploder($request->file('report_files'), $request->case_id);
                            if ($response["status"] == 1) {
                                $additional_parameter->report_files = $response["files"];
                            }
                        }

                        if ($request->hasfile('bill_files')) {
                            $response = $this->filesUploder($request->file('bill_files'), $request->case_id);
                            if ($response["status"] == 1) {
                                $additional_parameter->bill_files = $response["files"];
                            }
                        }

                        if ($request->hasfile('other_files')) {
                            $response = $this->filesUploder($request->file('other_files'), $request->case_id);
                            if ($response["status"] == 1) {
                                $additional_parameter->other_files = $response["files"];
                            }
                        }
                    /*********** End Uploading Files ***********/

                    $activity->message = $message;
                    $activity->type = 1;
                    $activity->claim_id = $request->case_id;
                    $activity->additional_parameter = $additional_parameter;
                    $activity = $this->addActivity($activity);
                /*********** End Create Activity ***********/

                Session::flash('success-messages', ['Case Status Successfully Updated.']);
                $response = array('status' => 1, 'message' => "Case Status Successfully Updated.", "activity" => $activity);
            }else {
                $response = array('status' => 0, 'message' => "Something went wrong");   
            }
        } catch (Exception $e) {
            $response = array('status' => 0, 'message' => "Something went wrong");   
        }
        return response()->json($response);
    }

    public function deleteCase($case_id)
    {
       try {
            $claim = Claim::where('id', $case_id)->first();
            $claim->is_deleted = 1;
            $claim->save();
            Session::flash('success-messages', ['Case Deleted.']);
            return redirect('case/all');
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }

    public function addActivity($data)
    {
        try {
            $activity = new Activity();
            $activity->type = $data->type;
            $activity->message = $data->message;
            $activity->claim_id = $data->claim_id;
            $activity->additional_parameter = json_encode($data->additional_parameter);
            $activity->created_by = Session::get('user_id');
            $activity->save();
            return $activity;
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');   
        }
    }


    public function filesUploder($files, $case_id)
    {
        try {
            $result = array();
            foreach ($files as $i => $file) {
                if($file->isValid()) {
                    $ext = $file->guessExtension();
                    $name = 'claim_'.$case_id.'_'.microtime().uniqid().".{$ext}";
                    $file->move('reports/', $name);
                    $result[] = array("name" => $file->getClientOriginalName(), 'size' => $file->getClientSize(), "type" => $ext, "url" => 'reports/'.$name, 'preview' => $this->getFileimage($ext));
                    $activity_type = 1;
                }
            }
            return array('status' => 1, 'message' => "File uploded", "files" => $result);   
        } catch (Exception $e) {
            return array('status' => 0, 'message' => $e->getMessage());   
        }
    }
}