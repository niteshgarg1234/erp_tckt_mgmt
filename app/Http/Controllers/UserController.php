<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
use App\Models\Insurer;
use App\Models\ClaimType;
use App\Models\Claim;
use App\Models\Activity;
use App\Models\Notification;

use App\Models\UserType;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\RoleRequest;
use App\Http\Requests\UpdatePassword;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\CreateCaseRequest;
use App\Http\Requests\CaseAssignedUserRequest;
use App\Http\Requests\CaseStatus;
use App\Http\Requests\FieldWork;
use App\Http\Requests\CaseReport;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('isUser', ['only' => ['changePassword', 'updateUserPassword', 'myProfile', 'updateMyProfile', ]]);
        $this->middleware('isAdmin', ['only' => ['updateUserDetails', 'deleteUser', 'all', 'roleAndPermissionMapping', 'roleSave', 'save', 'editUser', 'create', 'role',]]);
    }
    public function index()
    {
    	return view('dashboard');
    }

    public function profile()
    {
    	return view('profile');
    }

    public function create()
    {
    	$roles = Role::all();
    	$types = UserType::all();
    	return view('user.create', compact(['roles', 'types']));
    }
    

  
    public function role()
    {
        return view('user.role');
    }
   
    public function changePassword() {
        return view('user.change-password');
    }
   
    public function updateUserPassword(UpdatePassword $request) {
        try {
            $user_id = Session::get('user_id');
            $user = User::where('id', $user_id)->first();
            if ($user) {
                if (Hash::check($request->old_password, $user->password)) {
                    $user->password  = Hash::make($request->new_password);
                    $user->save();
                    Session::flash('success-messages', ['Password updated successfully.']);
                    return redirect('/user/change-password');
                }
                return view('user.change-password')->withErrors('Old password not matched.');
            }
            return view('user.change-password')->withErrors("User not found.");
        } catch (Exception $e) {
            return $redirect->back()->withErrors('Something went wrong.');
        }
    }

    public function myProfile()
    {
        try {
            $user_id = Session::get('user_id');
            $user = User::where('id', $user_id)->first();
            
            return view('user.my-profile', compact(['user']));
        } catch (Exception $e) {
            return redirect()->back()->withError("Something went wrong.");
        }
    }

    public function updateMyProfile(ProfileRequest $request)
    {
        try {
            $user_id = Session::get('user_id');
            $user = User::where('id', $user_id)->first();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->phone = $request->phone;
            $user->dob = $request->dob;
            $user->doj = $request->doj;

            $user->save();
            return view('user.my-profile', compact(['user']));
        } catch (Exception $e) {
            return redirect()->back()->withError("Something went wrong.");
        }
    }

    public function editUser($user_id)
    {
        try {
            $user = User::where('id', $user_id)->first();
            
            return view('user.edit-user', compact(['user']));
        } catch (Exception $e) {
            return redirect()->back()->withError("Something went wrong.");
        }   
    }
    
    public function updateUser(ProfileRequest $request, $user_id)
    {
        try {
            $user = User::where('id', $user_id)->first();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->dob = $request->dob;
            $user->doj = $request->doj;

            $user->save();
            return view('user.my-profile', compact(['user']));
        } catch (Exception $e) {
            return redirect()->back()->withError("Something went wrong.");
        }
    }

    
    public function save(RegistrationRequest $request)
    {
    	try {
            $user = User::where('email', $request->email);
            if ($user) {
                Session::flash('error-messages', ['E-mail already exist.']);
                return redirect()->back()->withErrors('E-mail already exist.');            
            }
    		$user = new User();
    		$user->first_name = $request->first_name;
    		$user->last_name = $request->last_name;
    		$user->email = $request->email;
    		$user->phone = $request->phone;
    		$user->address = $request->address;
    		$user->dob = $request->dob;
    		$user->doj = $request->doj;
    		$user->role = $request->role;
    		$user->type = $request->type;
    		$user->password = Hash::make($request->password);
    		$user->save();
            
    		return redirect('/user/all');
    	}catch(Exception $e) {
    		return redirect()->back()->withErrors('Something went wrong');
    	}
    }
    


    public function all()
    {
    	$users = User::join('user_types', 'user_types.id', '=', 'users.type')
    				->join('roles', 'roles.id', '=', 'users.role')
    				->select('users.*', 'roles.role AS role_name', 'user_types.type AS type_name')->where('users.is_deleted', 0)->paginate();
    	return view('user.all', compact(['users']));
    }

    public function updateUserDetails(Request $request, $user_id)
    {
        try {
            $user = User::where('id', $user_id)->first();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->dob = $request->dob;
            $user->doj = $request->doj;
            $user->address = $request->address;

            $user->save();
            return view('user.my-profile', compact(['user']));
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function roleAndPermissionMapping()
    {
        try {
            $roles = Role::where('is_deleted', 0)->get();
            $permissions = Permission::all();
            return view('user.role-permission-mapping', compact(['roles', 'permissions']));
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function roleSave(RoleRequest $request)
    {
        try {
            $role = new Role();
            $role->role = $request->role_name;
            $role->save();
            return redirect('/report/overview');
        }
        catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function getRolePermitions(RoleRequest $request)
    {
        try {
            // SELECT permission_role_relations.permission_id FROM permission_role_relations WHERE role_id= 1
            $permissions = '';
            return response()->json($permissions)->header("Content-Type", "appalication/json");
        }
        catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function deleteUser($user_id)
    {
        try {
            $user = User::where('id', $user_id)->first();
            $user->is_deleted = 1;
            $user->save();

            return redirect('/user/all');
        } catch (Exception $e) {
            return redirect()->back()->withError("Something went wrong.");
        }
    }

}