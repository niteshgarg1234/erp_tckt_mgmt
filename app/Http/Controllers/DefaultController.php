<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class DefaultController extends Controller {
    
    public function auth(LoginRequest $request)
    {
        try {
            $user = User::where(['email' => $request->email])->first();
            if($user) {
                if(Hash::check($request->password, $user->password)) {
                    Session::put('user_id', $user->id);
                    Session::put('user_role', $user->role);
                    Session::put('user_name', $user->first_name . ' ' . $user->last_name);
                    return redirect('/case/all');
                }else {
                    return redirect()->back()->withErrors('Username password does not match');
                }
            }else {
                return redirect()->back()->withErrors('User not found');
            }
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function logout()
    {
        try {
            Session::flush();
            return redirect('/login');
        } catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }
    
    public function forget(Request $request)
    {
        try {
            $user = User::where(['email' => $request->email])->first();
            if($user) {
                $code = md5(time().uniqid());
                $to = $request->email;
                $from = "noreply@maccoy.in"; 
                $Subject="Macoy Forgot Password Request";
                $Header = "From: ".$from."\r\n"; 
                $Header .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
                $Body='<p><span style="background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)">Dear User, </span> <br> </p>
                <p><span style="background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)">We have received your password reset request. </span>
                <br>
                </p>
                <p><span style="background-color:rgb(255, 255, 255); color:rgb(34, 34, 34)">Following this link to reset your password:</span> <br> </p>
                <p><a href="'.$_POST["Name"].'"> Click Here </a> </p>';

                $sender = mail($to, $Subject, $Body, $Header);
                if ($sender) {
                    return redirect()->back()->withErrors('A password reset link has been sent to your e-mail.');
                } else {
                    return redirect()->back()->withErrors('Error: pelase try again later.');
                }
            } else {
                return redirect()->back()->withErrors('User not found');
            }
        }catch(Exception $e) {
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

    public function contact(Request $request)
    {
        try {
            $name = $request->name;
            $email = $request->email;
            $message = $request->message;
            $this->sendMail('admin@maccoy.co.in', 'Maccoy - New Request', "New User Contact Request <br> Name: {$name} <br> Email Id: {$email} <br> Message: {$message}.");
        } catch (Exception $e) {   
            return redirect()->back()->withErrors('Something went wrong');
        }
    }

}
