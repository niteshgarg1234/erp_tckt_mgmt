<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class FieldOfficer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!(Session::has('user_role') && ( Session::get('user_role') == 3 || Session::get('user_role') == 2 || Session::get('user_role') == 1 ))) {
            return redirect('/login');
        }
        return $next($request);
    }
}
