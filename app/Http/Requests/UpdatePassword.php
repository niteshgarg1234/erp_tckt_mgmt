<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdatePassword extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password'  =>  'required|string|min:6',
            'confirm_password'  =>  'required|string|min:6',
            'new_password'  =>  'required|same:confirm_password',
        ];
    }
}
