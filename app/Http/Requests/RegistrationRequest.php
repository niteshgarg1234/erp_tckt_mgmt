<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        =>  'required|string',
            'last_name'         =>  'required|string',
            'email'             =>  'required|email',
            'phone'             =>  'required|string',
            'dob'               =>  'required|date',
            'doj'               =>  'required|date',
            'password'          =>  'required|string|min:6',
            'confirm_password'  =>  'required|same:password',
        ];
    }
}
