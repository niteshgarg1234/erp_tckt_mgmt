<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateCaseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'doi'   =>  'required|string',
            'insurer'   =>  'required|integer',
            'claim_type'    =>  'required|integer',
            'clid'  =>  'required_if:insurer,2|string',
            'foreign_id'    =>  'required|string', // Claim no.
            'native_reference'  =>  'required|string',
            // 'policy_no' =>  'required|string',
            // 'insurer_name'  =>  'required|string',
            // 'hospital_name' =>  'required|string',
            // 'location'  =>  'required|string',
            // 'claim_amount'  =>  'required|string',
            // 'triggers'  =>  'required|string'
        ];
    }
}