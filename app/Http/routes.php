<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});


Route::get('/login', function () {
	return view('login');
});

Route::post('/contact', 'DefaultController@contact');

Route::post('/forget', 'DefaultController@forget');

Route::post('/auth', 'DefaultController@auth');
Route::any('/logout', 'DefaultController@logout');

Route::get('/report/overview', 'UserController@index');
Route::get('/profile', 'UserController@profile');

Route::get('/user/change-password', 'UserController@changePassword');
Route::post('/user/change-password', 'UserController@updateUserPassword');
Route::get('/user/my-profile', 'UserController@myProfile');
Route::post('/user/my-profile', 'UserController@updateMyProfile');

Route::get('/user/create', 'UserController@create');
Route::get('/role/create', 'UserController@role');
Route::post('/role/create', 'UserController@roleSave');
Route::post('/user/save', 'UserController@save');
Route::get('/user/{id}/edit', 'UserController@editUser');
Route::post('/user/{id}/edit', 'UserController@updateUserDetails');
Route::get('/user/{id}/delete', 'UserController@deleteUser');
Route::get('/user/all', 'UserController@all');

Route::get('/role-permission-mapping', 'UserController@roleAndPermissionMapping');


Route::get('/case/create', 'ClaimCotroller@caseCreate');
Route::post('/case/create', 'ClaimCotroller@createCase');
Route::get('/case/{id}/edit', 'ClaimCotroller@editCase');
Route::get('/case/{id}/view', 'ClaimCotroller@viewCase');
Route::post('/case/{id}/udpate', 'ClaimCotroller@updateCase');
Route::get('/case/all', 'ClaimCotroller@manageCases');
Route::post('/case/all', 'ClaimCotroller@filterCases');

Route::post('/case/create-report', 'ClaimCotroller@attachCaseReport');
Route::post('/case/fw-work-complete', 'ClaimCotroller@caseFieldWorkComplete');
Route::post('/case/assign-user/',  'ClaimCotroller@updateCaseAssigned');
Route::post('/case/update-status/', 'ClaimCotroller@updateCaseStatus');
Route::get('/case/{id}/delete', 'ClaimCotroller@deleteCase');

Route::get('/manage-claim-type', 'ClaimTypeController@index');
Route::post('/manage-claim-type/add', 'ClaimTypeController@create');
Route::get('/manage-claim-type/{id}/edit', 'ClaimTypeController@edit');
Route::post('/manage-claim-type/{id}/update', 'ClaimTypeController@update');
Route::get('/manage-claim-type/{id}/delete', 'ClaimTypeController@destroy');


Route::get('/manage-insurer', 'InsurerController@index');

Route::post('/manage-insurer/add', 'InsurerController@store');
Route::get('/manage-insurer/{id}/edit', 'InsurerController@edit');
Route::get('/manage-insurer/{id}/delete', 'InsurerController@destroy');
Route::post('/manage-insurer/{id}/update', 'InsurerController@update');