<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Insurer extends Model
{
	public static function getInsures()
	{
	    return DB::table('insurers')
	            ->leftJoin('users', 'users.id', '=', 'insurers.created_by')
                ->selectRaw('insurers.*, IFNULL(CONCAT(users.first_name, " ", users.last_name), "--") AS user_name')
                ->where('insurers.is_deleted', 0)
	            ->get();
	}
}
