<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class ClaimType extends Model
{
    public static function getClaimTypes()
	{
	    return DB::table('claim_types')
	            ->leftJoin('users', 'users.id', '=', 'claim_types.created_by')
                ->selectRaw('claim_types.*, IFNULL(CONCAT(users.first_name, " ", users.last_name), "--") AS user_name')
                ->where('claim_types.is_deleted', 0)
	            ->get();
	}
}
