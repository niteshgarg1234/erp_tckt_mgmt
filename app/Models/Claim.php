<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Claim extends Model
{
    
    public static function getAll()
    {
    	return DB::table('claims')
	            ->selectRaw('claims.*, IFNULL(CONCAT(users.first_name, " ", users.last_name), "--") AS assigned_user, insurers.short_name AS insurer_short_name, (datediff(now(),claims.created_at)+1) as tat')
	            ->leftJoin('users', 'users.id', '=', 'claims.assigned')
	            ->join('insurers', 'insurers.id', '=', 'claims.insurer')
	            ->join('claim_types', 'claim_types.id', '=', 'claims.claim_type')
    			->where('claims.is_deleted', 0)
    			->whereNotIn('claims.status', [6])
	            ->get();
    }
}
