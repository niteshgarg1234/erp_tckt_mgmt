<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('tat');
            $table->tinyInteger('is_deleted');
            $table->integer('created_by');
            $table->timestamps();
        });

        DB::table('claim_types')->insert([
            ['type' => 'Reimbursement', 'tat' => 168],
            ['type' => 'Cashless', 'tat' => 8],
            ['type' => 'Reimbursement Intimation', 'tat' => 8],
            ['type' => 'Motor PA', 'tat' => 168],
            ['type' => 'PA', 'tat' => 168],
            ['type' => 'GPA', 'tat' => 168],
            ['type' => 'PBBY', 'tat' => 168],
            ['type' => 'PMSBY', 'tat' => 168],
            ['type' => 'TP Injury', 'tat' => 168],
            ['type' => 'SBI PA', 'tat' => 168],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claim_types');
    }
}
