<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Hash;

class AddAdminInUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
            'first_name' => "Admin",
            'last_name' => "Demo",
            'email' => "admin@maccoy.co.in",
            'phone' => "0000000000",
            'password' => Hash::make('789456123'),
            'address' => "Ghar me",
            'role' => 1,
            'status' => 0,
            'dob' => "CURRENT_TIMESTAMP",
            'doj' => "CURRENT_TIMESTAMP",
            'updated_at' => "CURRENT_TIMESTAMP",
            'created_at' => "CURRENT_TIMESTAMP"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
