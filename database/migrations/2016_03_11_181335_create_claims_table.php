<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->date('doi');
            $table->integer('user_id')->unsigned();
            $table->integer('insurer')->unsigned();
            $table->integer('claim_type')->unsigned();
            $table->string('clid');
            $table->string('foreign_id');
            $table->string('policy_no');
            $table->string('native_reference');
            $table->string('insurer_name');
            $table->string('hospital_name');
            $table->string('location');
            $table->decimal('claim_amount', 11, 3);
            $table->string('triggers');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('insurer')->references('id')->on('insurers');
            $table->foreign('claim_type')->references('id')->on('claim_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claims');
    }
}
