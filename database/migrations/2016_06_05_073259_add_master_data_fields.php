<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMasterDataFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('insurers', function (Blueprint $table) {
            $table->string('address');
            $table->integer('created_by');
        });
        Schema::table('claim_types', function (Blueprint $table) {
            $table->integer('created_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('insurers', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('created_by');
        });
        Schema::table('claim_types', function (Blueprint $table) {
            $table->dropColumn('created_by');
        });
    }
}
