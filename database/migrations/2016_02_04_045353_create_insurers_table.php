<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('short_name');
            $table->text('address');
            $table->integer('created_by');
            $table->tinyInteger('is_deleted');
            $table->timestamps();
        });

        DB::table('insurers')->insert([
            ['name' => 'Reliance General Insurance Co. Ltd.', 'short_name' => 'RGICL'],
            ['name' => 'Bajaj Allianz General Insurance Co. Ltd.', 'short_name' => 'BAGICL'],
            ['name' => 'HDFC ERGO General Insurance Co. Ltd.', 'short_name' => 'HDFC'],
            ['name' => 'Religare Health Insurance Co. Ltd.', 'short_name' => 'RHICL'],
            ['name' => 'L & T Insurance Company', 'short_name' => 'LNTIC'],
            ['name' => 'Universal Sompo General Insurance Co. Ltd.', 'short_name' => 'USGICL'],
            ['name' => 'Future Generali Health Insurance Company', 'short_name' => 'FGHIC']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('insurers');
    }
}
