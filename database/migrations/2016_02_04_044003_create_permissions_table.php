<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('permission');
            $table->timestamps();
        });

        DB::table('permissions')->insert([
            ['permission' => 'Create User'],
            ['permission' => 'Create Case'],
            ['permission' => 'Field Work Completion'],
            ['permission' => 'Final Report Creation'],
            ['permission' => 'Invoice Creation'],
            ['permission' => 'Invoice Sent'],
            ['permission' => 'Payment Status'],
            ['permission' => 'Report View'],
            ['permission' => 'Hard Copy Status'],
            ['permission' => 'Upload Supporting Documents']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permissions');
    }
}
