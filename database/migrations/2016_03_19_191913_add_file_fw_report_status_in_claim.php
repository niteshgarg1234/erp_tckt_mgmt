<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileFwReportStatusInClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('claims', function (Blueprint $table) {
            $table->integer('status');
            $table->string('attached_report');
            $table->string('fw_report');
            $table->text('fw_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('claims', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('attached_report');
            $table->dropColumn('fw_report');
            $table->dropColumn('fw_details');
        });
    }
}
