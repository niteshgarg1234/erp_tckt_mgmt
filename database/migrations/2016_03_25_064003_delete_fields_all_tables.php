<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteFieldsAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('is_deleted');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->integer('is_deleted');
        });
        
        Schema::table('permissions', function (Blueprint $table) {
            $table->integer('is_deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });
    }
}
