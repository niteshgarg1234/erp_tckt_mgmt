var $status = ['Pending', 'Assigned', 'FW Completed', 'Report Attached', 'Invoice Pending', 'Payment Pending', 'Payment Done'];

var Case = {
	UpdateStatusEvent: function() {
		$('.update-case-status').off('click');
		$('.update-case-status').on('click', function(e) {
			e.preventDefault();
			var case_id = $(this).parents('tr:eq(0)').data('id');
			var status = $(this).parents('tr:eq(0)').data('status');
			console.log(status);
			$('#update-case-status-modal form').hide();
			switch(parseInt(status)) {
				case 0: // Case is pending lets assign a Field Officer
					$('#assign-case-to-field-officer-form #case_id').val(case_id);
					$('#assign-case-to-field-officer-form').show();
					$('#update-case-status-modal').modal('show');
				break;

				case 1: // Complete case Field Work
					$('#end-fw-work-form #case_id').val(case_id);
					$('#end-fw-work-form').show();
					$('#update-case-status-modal').modal('show');
				break;

				case 2: // Create case Report According to Field Work Report
					$('#create-case-report-form #case_id').val(case_id);
					$('#create-case-report-form').show();
					$('#update-case-status-modal').modal('show');
				break;

				case 3: // Mail Sent to Client
					$('#end-case-form #case_id').val(case_id);
					$('#end-case-form').show();

					$('#case_status').val(4);
					$('#update-status-text').text($('#case_status option:selected').text());
					$('#update-case-status-modal').modal('show');
				break;

				case 4: // Payment Recieved from Client
					$('#end-case-form #case_id').val(case_id);
					$('#end-case-form').show();

					$('#case_status').val(5);
					$('#update-status-text').text($('#case_status option:selected').text());
					$('#update-case-status-modal').modal('show');
				break;

				case 5: // Close Case
					$('#end-case-form #case_id').val(case_id);
					$('#end-case-form').show();
					$('#case_status').val(6);
					$('#update-status-text').text($('#case_status option:selected').text());
					$('#update-case-status-modal').modal('show');
				break;

				default:
				break;
			}
		});
	},

	deleteEvent: function() {
		$('.delete-claim').off('click');
		$('.delete-claim').on('click', function(e) {
			// var claim = $(this).parents('tr').find('td:0').text();
			if (!confirm('Are you sure want to delete this')) {
				e.preventDefault();
			};
		});
	}, 
	filter: function() {
		$('#filter-cases-form').off('submit');
		$('#filter-cases-form').on('submit', function(e) {
			e.preventDefault();
			var status = $('#status-filter').val();
			var insurer = $('#insurer').val();
			var claim_type = $('#claim_type').val();
			var assigned_user = $('#assigned_user').val();

			var tat = $('#tat').val();
			var _token = $('#filter-cases-form ._token').val();

			var request = {}
			request._token = _token;
			if (status != -1) {
				request.status = status;
			}
			if (insurer != 0) {
				request.insurer = insurer;
			}
			if (claim_type != 0) {
				request.claim_type = claim_type;
			}
			if (tat != 0) {
				request.tat = tat;
			}
			if (assigned_user != 0) {
				request.assigned_user = assigned_user;
			}
			
			
			$.ajax({
				type: "post",
				url: EndPoint.FilterCase,
				data: request
			}).done(function(response) {
				if (response.status) {
					Case.fillCases(response.claims);
					toastr.clear();
					toastr.success(response.message);
				}else {
					toastr.clear();
					toastr.error(response.message);
				}
			});
		});
	},
	fillCases: function(cases) {
		for (var i = 0; i < cases.length; i++) {
			console.log(cases[i]);
		}
	},
	init: function() {
		Case.UpdateStatusEvent();
		Case.deleteEvent();
		Case.filter();
		Case.UpdateCaseStatusEvent();
	}
};


Case.fillCases = function(cases) {
	var html = '';
	if(typeof Table != "undefined") {
		Table.fnDestroy();
	}
	
	for (var i = 0; i < cases.length; i++) {
		var status = (cases[i].status > 1)? (cases[i].fw_report == 1)? 'true-case' : 'false-case'  : '';
		html += '<tr class="'+status+'" data-id="'+cases[i].id+'" data-status="'+cases[i].status+'"  data-assigned="'+cases[i].assigned+'">\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].doi+'">'+cases[i].doi+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].native_reference+'">'+cases[i].native_reference+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].insurer_short_name+'">'+cases[i].insurer_short_name+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].foreign_id+'">'+cases[i].foreign_id+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].insurer_name+'">'+cases[i].insurer_name+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].hospital_name+'">'+cases[i].hospital_name+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].assigned_user+'">'+cases[i].assigned_user+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+$status[cases[i].status]+'">'+$status[cases[i].status]+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].claim_amount+'">'+cases[i].claim_amount+'</td>\
					<td data-container="body" data-placement="top" data-original-title="'+cases[i].tat+'">'+cases[i].tat+'</td>\
					<td>\
						<button data-container="body" data-placement="top" data-html="true" data-original-title="Update <br> Status" class="btn btn-warning btn-xs tooltips update-case-status" ><i class="fa fa-gavel"></i></button>';
							if(cases[i].status > 1) {

								html += '<a data-container="body" data-placement="top" data-original-title="View" href="{{ url("/case/'+cases[i].id+'/view") }} " class="btn btn-primary tooltips btn-xs"><i class="fa fa-eye"></i></a>';
							}
							html += '<a data-container="body" data-placement="top" data-original-title="Edit" href="{{ url("/case/'+cases[i].id+'/edit") }} " class="btn btn-primary tooltips btn-xs edit"><i class="fa fa-pencil"></i></a>\
							<a data-container="body" data-placement="top" data-original-title="Delete" href=" {{ url("/case/'+cases[i].id+'/delete") }} " class="delete-claim btn tooltips btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>\
					</td>\
				</tr>';
	}
	$('#manage-case-table tbody').html(html);
	Table = $('#manage-case-table').dataTable( {
	    "aaSorting": [[ 1, "desc" ]],
	});
}


Case.UpdateCaseStatusEvent = function() {
	$('#assign-case-to-field-officer-form').off('submit');
	$('#assign-case-to-field-officer-form').on('submit', function(e) {
		e.preventDefault();
		if (true) {
			$.ajax({
				type: 'post',
				url:  EndPoint.AssignFieldOfficer,
				processData: false,
            	contentType: false,
				data: new FormData($(this).get(0))
			}).done(function(response) {
				if(response.status == 1) {
					toastr.success(response.message);
					$('#update-case-status-modal').modal('hide');
					window.location.reload();
				}
				else{
					toastr.error(response.message);
				}
			});
		}
	});

	$('#end-fw-work-form').off('submit');
	$('#end-fw-work-form').on('submit', function(e) {
		e.preventDefault();
		if (true) {
			$.ajax({
				type: 'post',
				url:  EndPoint.EndFieldWork,
				processData: false,
            	contentType: false,
				data: new FormData($(this).get(0))
			}).done(function(response) {
				if(response.status == 1) {
					toastr.success(response.message);
					$('#update-case-status-modal').modal('hide');
					window.location.reload();
				}
				else{
					toastr.error(response.message);
				}
			});
		}
	});

	$('#create-case-report-form').off('submit');
	$('#create-case-report-form').on('submit', function(e) {
		e.preventDefault();
		if (true) {
			$.ajax({
				type: 'post',
				url:  EndPoint.CreateReport,
				processData: false,
            	contentType: false,
				data: new FormData($(this).get(0))
			}).done(function(response) {
				if(response.status == 1) {
					toastr.success(response.message);
					$('#update-case-status-modal').modal('hide');
					window.location.reload();
				}
				else{
					toastr.error(response.message);
				}
			});
		}
	});

	$('#end-case-form').off('submit');
	$('#end-case-form').on('submit', function(e) {
		e.preventDefault();
		if (true) {
			$.ajax({
				type: 'post',
				url:  EndPoint.UpdateStatus,
				processData: false,
            	contentType: false,
				data: new FormData($(this).get(0))
			}).done(function(response) {
				if(response.status == 1) {
					toastr.success(response.message);
					$('#update-case-status-modal').modal('hide');
					window.location.reload();
				}
				else{
					toastr.error(response.message);
				}
			});
		}
	});
}