@extends('master')

@section('title')
Profile
@stop

@section('content')
<section class="wrapper">
    <!-- page start-->
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="img/profile-avatar.jpg" alt="">
                    </a>
                    <h1>Jonathan Smith</h1>
                    <p>jsmith@flatlab.com</p>
                </div>

                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="profile.html"> <i class="fa fa-user"></i> Profile</a></li>
                    <li><a href="profile-activity.html"> <i class="fa fa-calendar"></i> Recent Activity <span class="label label-danger pull-right r-activity">9</span></a></li>
                    <li><a href="profile-edit.html"> <i class="fa fa-edit"></i> Edit profile</a></li>
                </ul>

            </section>
        </aside>
        <aside class="profile-info col-lg-9">
          <section class="panel">
              <div class="bio-graph-heading">
                Bio Graph
                <a href="#" class="btn btn-default pull-right btn-xs"><i class="fa fa-pencil"></i></a>
              </div>
              <div class="panel-body bio-graph-info">
                  <div class="row">
                      <div class="bio-row">
                          <p><span>Name </span>: {{ $user->name }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>Email </span>: {{ $user->email }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>Mobile</span>: {{ $user->mobile }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>Designation </span>: {{ $user->role }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>DOB  </span>: {{ $user->dob }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>DOB  </span>: {{ $user->dob }}</p>
                      </div>
                      <div class="bio-row">
                          <p><span>Phone </span>: 88 (02) 123456</p>
                      </div>
                  </div>
              </div>
          </section>
          <section>
              <div class="row">
                  <div class="col-lg-6">
                      <div class="panel">
                          <div class="panel-body">
                              <div class="bio-chart">
                                  <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="35" data-fgColor="#e06b7d" data-bgColor="#e8e8e8">
                              </div>
                              <div class="bio-desk">
                                  <h4 class="red">Envato Website</h4>
                                  <p>Started : 15 July</p>
                                  <p>Deadline : 15 August</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="panel">
                          <div class="panel-body">
                              <div class="bio-chart">
                                  <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="63" data-fgColor="#4CC5CD" data-bgColor="#e8e8e8">
                              </div>
                              <div class="bio-desk">
                                  <h4 class="terques">ThemeForest CMS </h4>
                                  <p>Started : 15 July</p>
                                  <p>Deadline : 15 August</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="panel">
                          <div class="panel-body">
                              <div class="bio-chart">
                                  <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="75" data-fgColor="#96be4b" data-bgColor="#e8e8e8">
                              </div>
                              <div class="bio-desk">
                                  <h4 class="green">VectorLab Portfolio</h4>
                                  <p>Started : 15 July</p>
                                  <p>Deadline : 15 August</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-6">
                      <div class="panel">
                          <div class="panel-body">
                              <div class="bio-chart">
                                  <input class="knob" data-width="100" data-height="100" data-displayPrevious=true  data-thickness=".2" value="50" data-fgColor="#cba4db" data-bgColor="#e8e8e8">
                              </div>
                              <div class="bio-desk">
                                  <h4 class="purple">Adobe Muse Template</h4>
                                  <p>Started : 15 July</p>
                                  <p>Deadline : 15 August</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
        </aside>
    </div>

    <!-- page end-->
</section>
@stop

@section('scripts')
<script src="{{ asset('/js/jquery.knob.js') }}"></script>
<script type="text/javascript">
  $(function() {$(".knob").knob();});
</script>
@stop