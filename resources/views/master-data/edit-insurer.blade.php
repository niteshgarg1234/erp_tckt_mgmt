@extends('master')

@section('title')
Insurer
@stop

@section('styles')
    <link rel="stylesheet" href="{{asset('js/data-tables/demo_table.css')}}" />
    <link rel="stylesheet" href="{{asset('js/data-tables/DT_bootstrap.css')}}" />
@stop

@section('content')
  <section class="wrapper site-min-height">
    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    Manage Insurers
                </header>
                <div class="panel-body">
                    <div class="col-md-12">
                    <form action="{{url('/manage-insurer/'.$insurer->id.'/update')}}" id="edit-insurer-form" name="edit-insurer-form" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="control-label">Name <span class="required">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Insurer Name" value="{{$insurer->name}}">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="control-label">Short Name <span class="required">*</span></label>
                            <input type="text" class="form-control" id="short-name" name="short-name" placeholder="Insurer Short Name" value="{{$insurer->short_name}}">
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label class="control-label">Address <span class="required">*</span></label>
                            <textarea class="form-control" id="address" name="address" placeholder="Insurer Address" rows="3">{{$insurer->address}}</textarea>
                          </div>
                        </div>
                        <div class="col-lg-1 pull-right">
                          <div class="form-group">
                            <button type="submit" class="btn btn-success"> Save </button>
                          </div>
                        </div>
                        <div class="col-lg-1 pull-right">
                          <div class="form-group">
                            <a href="{{url('/manage-insurer')}}"class="btn btn-danger">Cancel</a>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-12">
                    <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="insurer-table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Short Name</th>
                                    <th>Address</th>
                                    <th>Created by</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($insurers as $insurer)
                                  <tr data-id="{{$insurer->id}}" class="gradeX">
                                      <td>{{$insurer->name}}</td>
                                      <td>{{$insurer->short_name}}</td>
                                      <td>{{$insurer->address}}</td>
                                      <td>{{$insurer->user_name}}</td>
                                      <td>
                                          <a href="{{url('/manage-insurer/'.$insurer->id.'/edit')}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                          <a href="{{url('/manage-insurer/'.$insurer->id.'/delete')}}"  class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                        </td>
                                  </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
  </section>
@stop

@section('scripts')
  <script type="text/javascript" language="javascript" src="{{asset('js/data-tables/jquery.dataTables.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/data-tables/DT_bootstrap.js')}}"></script>

  <script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#nav-accordion #master-data-li a:eq(0)').click();
        $('#nav-accordion #master-data-li ul:eq(0) #insurer-li').addClass("active").parents('.sub-menu').addClass("active");
        
        $('#insurer-table').dataTable( {
          "aaSorting": [[ 1, "desc" ]]
        });
    } );
  </script>
@stop

