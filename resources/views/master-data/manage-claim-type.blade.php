@extends('master')

@section('title')
Claim Type
@stop

@section('styles')
    <!-- <link rel="stylesheet" href="{{asset('js/data-tables/demo_page.css')}}" /> -->
    <link rel="stylesheet" href="{{asset('js/data-tables/demo_table.css')}}" />
    <link rel="stylesheet" href="{{asset('js/data-tables/DT_bootstrap.css')}}" />
@stop


@section('content')
  <section class="wrapper site-min-height">
    <!-- page start-->
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                  Manage Claim Types
                </header>
                <div class="panel-body">
                  <div class="col-md-12">
                    <form action="{{url('/manage-claim-type/add')}}" id="add-claim-type-form" name="add-claim-type-form" method="post" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="control-label">Name <span class="required">*</span></label>
                            <input type="text" class="form-control" id="claim-type-name" name="type" placeholder="Name">
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label class="control-label">TAT <span class="required">*</span></label>
                            <input type="text" class="form-control" id="claim-type-tat" name="tat" placeholder="TAT">
                          </div>
                        </div>
                        <div class="col-lg-1 pull-right">
                          <div class="form-group">
                            <button type="submit" class="btn btn-success"> Save </button>
                          </div>
                        </div>
                        <!-- <div class="col-lg-1 pull-right">
                          <div class="form-group">
                            <a href="#"class="btn btn-danger">Cancel</a>
                          </div>
                        </div> -->
                      </div>
                    </form>
                  </div>
                  <div class="col-md-12">
                    <div class="adv-table">
                      <table  class="display table table-bordered table-striped" id="claim-type-table">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <th>TAT</th>
                            <th>Created by</th>
                            <th>Action</th>
                            <!-- <th class="hidden-updated_at">Modified at</th> -->
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($claim_types as $claim_type)
                            <tr data-id="{{$claim_type->id}}" class="gradeX">
                                <td>{{$claim_type->type}}</td>
                                <td>{{$claim_type->tat}}</td>
                                <td>{{$claim_type->user_name}}</td>
                                <td>
                                  <a href="{{url('/manage-claim-type/'.$claim_type->id.'/edit')}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                  <a href="{{url('/manage-claim-type/'.$claim_type->id.'/delete')}}"  class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                                </td>
                                <!-- <td class="center hidden-updated_at">{{$claim_type->updated_at}}</td> -->
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
            </section>
        </div>
    </div>
    <!-- page end-->
  </section>
@stop

@section('scripts')
    <script type="text/javascript" language="javascript" src="{{asset('js/data-tables/jquery.dataTables.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/data-tables/DT_bootstrap.js')}}"></script>


      <script type="text/javascript" charset="utf-8">
          $(document).ready(function() {
            $('#nav-accordion #master-data-li a:eq(0)').click();
            $('#nav-accordion #master-data-li ul:eq(0) #clame-type-li').addClass("active").parents('.sub-menu').addClass("active");
              $('#claim-type-table').dataTable( {
                  "aaSorting": [[ 1, "desc" ]]
              });
          } );
      </script>

@stop
