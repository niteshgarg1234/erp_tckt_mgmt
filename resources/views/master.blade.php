<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="charset" content="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="CYberoze">
    <link rel="shortcut icon" href="{{ asset('/images/favicon.png') }}">

    <title>Maccoy Consultancy Services - @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/bootstrap-reset.css') }}" rel="stylesheet">
    <!--external css-->
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('/css/jquery.easy-pie-chart.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}" type="text/css">
    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style-responsive.css') }}" rel="stylesheet" />
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" />

    @yield('styles')

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header white-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="{{ url('/report/overview') }}" class="logo">M<span>C</span>S</a>
            <!--logo end-->
            
            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="username">{{ Session::get('user_name') }}</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>
                            <li><a href="{{ url('/user/my-profile') }}"><i class=" fa fa-suitcase"></i> My Profile</a></li>
                            <li><a href="{{ url('/user/change-password') }}"><i class=" fa fa-cogs"></i>Change Password</a></li>
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-key"></i> Log Out</a></li>
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li id="case-li" class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-file"></i>
                          <span>Cases</span>
                      </a>
                      <ul class="sub">
                          @if(Session::get('user_role') != 3)
                            <li id="create-case-li"><a  href="{{ url('/case/create') }}">Create Case</a></li>
                          @endif
                          <li id="manage-case-li"><a href="{{ url('/case/all') }}">All Cases</a></li>
                      </ul>
                  </li>
                  @if(Session::get('user_role') != 3)
                    <li id="master-data-li" class="sub-menu">
                        <a href="javascript:;" >
                            <i class="fa fa-asterisk"></i>
                            <span>Master Data</span>
                        </a>
                        <ul class="sub">
                            <li id="clame-type-li"><a  href="{{ url('/manage-claim-type') }}">Clame Type</a></li>
                            <li id="insurer-li"><a  href="{{ url('/manage-insurer') }}">Insurer</a></li>
                        </ul>
                    </li>
                    <li id="users-li" class="sub-menu">
                        <a href="javascript:;" >
                            <i class="fa fa-users"></i>
                            <span>Users</span>
                        </a>
                        <ul class="sub">
                            <li id="manage-users-li"><a href="{{ url('/user/all') }}">All Users</a></li>
                            <li id="create-users-li"><a href="{{ url('/user/create') }}">Create User</a></li>
                            <!-- <li><a  href="{{ url('/role-permission-mapping') }}"> Roles Permissions</a></li>
                            <li><a  href="{{ url('/role/create') }}">Create Role</a></li> -->
                        </ul>
                    </li>
                    <li id="reports-li" class="sub-menu">
                        <a href="javascript:;" >
                            <i class="fa fa-line-chart"></i>
                            <span>Reports</span>
                        </a>
                        <ul class="sub">
                            <li id="overview-reports-li"><a  href="{{ url('/report/overview') }}">Overview</a></li>
                            <li><a  href="{{ url('/report/sales') }}">Sales</a></li>
                            <li><a  href="{{ url('/report/traffic') }}">Traffic</a></li>
                        </ul>
                    </li>
                  @endif

                  <!--multi level menu start-->
                  <!-- <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-sitemap"></i>
                          <span>Multi level Menu</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="javascript:;">Menu Item 1</a></li>
                          <li class="sub-menu">
                              <a  href="boxed_page.html">Menu Item 2</a>
                              <ul class="sub">
                                  <li><a  href="javascript:;">Menu Item 2.1</a></li>
                                  <li class="sub-menu">
                                      <a  href="javascript:;">Menu Item 3</a>
                                      <ul class="sub">
                                          <li><a  href="javascript:;">Menu Item 3.1</a></li>
                                          <li><a  href="javascript:;">Menu Item 3.2</a></li>
                                      </ul>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                  </li> -->
                  <!--multi level menu end-->

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          @if($errors->any())
            <div class="col-md-9 col-md-offset-1">
            @foreach($errors->all() as $error)
              <div class="row alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{$error}}
              </div>
            @endforeach
            </div>
          @endif
         @yield('content')
      </section>
      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2016 &copy; Maccoy Consultancy Services.
              <a href="#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
              <span class="cyberoze">
                  Proudly Powered By: <a href="http://cyberoze.com" target="_blank">CYberoze.com</a>
              </span>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ url('/js/jquery.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ asset('/js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ asset('/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/jquery.sparkline.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/jquery.easy-pie-chart.js') }}"></script>
    <script src="{{ asset('/js/owl.carousel.js') }}" ></script>
    <script src="{{ asset('/js/jquery.customSelect.min.js') }}" ></script>
    <script src="{{ asset('/js/respond.min.js') }}" ></script>
    <script src="{{ asset('/js/toastr.min.js') }}" ></script>

    <!--common script for all pages-->
    <script src="{{ asset('/js/common-scripts.js') }}"></script>

    <!--script for this page-->
    <script src="{{ asset('/js/sparkline-chart.js') }}"></script>
    <script src="{{ asset('/js/easy-pie-chart.js') }}"></script>
    <script src="{{ asset('/js/count.js') }}"></script>

    <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem : true,
            autoPlay:true
          });
      });

      //custom select box

      $(function(){
        $('select.styled').customSelect();
        $('.tooltips').tooltip({html:true});
        @if(Session::has('success-messages'))
          @foreach(Session::get('success-messages') as $message)
            toastr.success('{{$message}}');
          @endforeach
        @endif
        @if(Session::has('error-messages'))
          @foreach(Session::get('error-messages') as $message)
            toastr.error('{{$message}}');
          @endforeach
        @endif
      });

    </script>
    @yield('scripts')

  </body>
</html>