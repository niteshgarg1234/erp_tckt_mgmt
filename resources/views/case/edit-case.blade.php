@extends('master')

@section('title')
Security
@stop

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{url('/js//bootstrap-datepicker/css/datepicker.css')}}"/>
@stop

@section('content')
<section class="wrapper ">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Edit Case
        </header>
        <div class="panel-body">
          @if(count($errors))
              @foreach($errors->all() as $error)
              <div class="row alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{$error}}
              </div>
              @endforeach
          @endif
          <div class="row">
            <form method="post" action="{{ url("/case/$claim->id/udpate") }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="doi">Date of Intimation: *</label>
                        <input type="text" name="doi" id="doi" readonly="" size="16" class="form-control input-sm" value="{{ $claim->doi }}" />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="insurer">Insurer *</label>
                      <select class="form-control input-sm" id="insurer" name="insurer">
                        <option value=""> Select Insurer</option>
                        @foreach($insurers as $insurer)
                          <option value="{{ $insurer->id }}" @if($insurer->id == $claim->insurer) {{ 'selected' }} @endif> {{ $insurer->name }} </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="claim_type">Claim Type: *</label>
                      <select class="form-control input-sm" name="claim_type" value="{{ $claim->claim_type }}">
                        <option value=""> Select Claim Type</option>
                        @foreach($claimTypes as $claimType)
                          <option value="{{ $claimType->id }}" @if($claimType->id == $claim->claim_type) {{ 'selected' }} @endif> {{ $claimType->type }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div id="clid-div" class="col-md-6" style="display: none;">
                    <div class="form-group">
                      <label for="clid"> CLID: *</label>
                      <input type="text" class="form-control input-sm" name="clid" id="clid" value="{{$claim->clid}}" placeholder="CLID *">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="claim_amount">Claim Amount: </label>
                      <input type="text" class="form-control input-sm" name="claim_amount" value="{{ $claim->claim_amount }}" placeholder="Claim Amount">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="foreign_id">Claim No. *</label>
                      <input type="text" class="form-control input-sm" name="foreign_id" value="{{ $claim->foreign_id }}" placeholder="Claim No.">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="policy_no">Policy No.: </label>
                      <input type="text" class="form-control input-sm" name="policy_no" value="{{ $claim->policy_no }}" placeholder="Policy No.">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="native_reference">Our reference no.</label>
                      <input type="text" class="form-control input-sm" name="native_reference" value="{{ $claim->native_reference }}" placeholder="Our reference no.">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="insurer_name">Insured Name: </label>
                      <input type="text" class="form-control input-sm" name="insurer_name" value="{{ $claim->insurer_name }}" placeholder="Insured Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="hospital_name">Hospital Name: </label>
                      <input type="text" class="form-control input-sm" name="hospital_name" value="{{ $claim->hospital_name }}" placeholder="Hospital Name">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="location">Location: </label>
                      <textarea class="form-control" name="location" placeholder="Location">{{ $claim->location }}</textarea>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="triggers">Triggers:</label>
                      <textarea class="form-control" name="triggers" placeholder="Triggers">{{ $claim->triggers }}</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <div class="pull-right">
                      <input type="submit" class="btn btn-sm btn-primary" value="Update">
                      <a onclick="history.back();" class="btn btn-sm btn-danger"> Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
      </section>
    </div>
  </div>
</section>
@stop

@section('scripts')
<script type="text/javascript" src="{{url('/js/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script>
    $(function() {
      showHideClid();
      $('#nav-accordion #case-li a:eq(0)').click();
      $('#nav-accordion #case-li ul:eq(0) #manage-case-li').addClass("active").parents('.sub-menu').addClass("active");
      $('#insurer').change();

      $('#doi').datepicker({
        format: "dd-mm-yyyy",
        defaultDate: new Date(),
        setStartDate: new Date,
        showMeridian: true,
        autoclose: true,
        todayHighlight: true
        // todayBtn: true
      });
    });

    function showHideClid() {
      $('#insurer').off('change');
      $('#insurer').on('change', function(e) {
        e.preventDefault();
        // $('#clid').val('');
        $('#clid-div').hide('slow');
        if ($(this).val() == '2') {
          $('#clid-div').show('slow');
        };
      });
    }
</script>
@stop
