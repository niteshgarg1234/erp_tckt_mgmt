@extends('master')

@section('title')
All Users
@stop

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{url('/js/data-tables/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
	<link rel="stylesheet" type="text/css" href="{{url('/js/data-tables/datatables/extensions/TableTools/css/dataTables.tableTools.min.css')}}"/>
	<style type="text/css">
		.dataTables_custom_search {
			margin-right: 10px;
			margin-top: -44px;
		}
	</style>
@stop


@section('content')
<section class="wrapper site-min-height">
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					All Cases
					@if(Session::get('user_role') != 3)
						<a href="{{ url('/case/create') }} " data-container="body" data-placement="top" data-original-title="Create Case" class="btn btn-sm btn-primary pull-right tooltips">
							<!-- <i class="fa fa-plus-square"></i>  -->Create Case
						</a>
					@endif
				</header>
				<div class="row">
					<div class="col-lg-12 margin-top-15">
						<form action="{{url('case/all')}}" id="filter-cases-form" method="post" enctype="multipart/form-data">
							<input type="hidden" class="_token" value="{{ csrf_token() }}">
							<div class="col-lg-3">
								<div class="form-group">
				        			<label for="select-user"> Select Field Officer </label>
				        			<select name="assigned_user" id="assigned_user" class="form-control" >
			        					<option value="0"> Select Field Officer </option>
				        				@foreach ($users as $i => $user)
				        					<option value="{{ $user->id }}"> {{ $user->first_name . ' ' . $user->last_name }} </option>
				        				@endforeach
				        			</select>
				        		</div>
			        		</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label> Status: </label>
									<select id="status-filter" class="form-control input-sm">
										<option value="-1"> All </option>
										<option value="0"> Pending </option>
										<option value="1"> Assigned </option>
										<option value="2"> FW Completed </option>
										<option value="3"> Report Created </option>
										<option value="4"> Invoice Pending </option>
										<option value="5"> Payment Pending </option>
									</select>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
			                     	<label for="insurer">Insurer</label>
			                     	<select class="form-control input-sm" id="insurer" name="insurer">
			                       		<option value="0"> Select Insurer</option>
				                        @foreach($insurers as $insurer)
				                          <option value="{{ $insurer->id }}"> {{ $insurer->name }} </option>
				                        @endforeach
			                     	</select>
			                    </div>
		                    </div>
							<div class="col-lg-2">
		                    	<div class="form-group">
			                     	<label for="claim_type">Claim Type:</label>
			                     	<select class="form-control input-sm" id="claim_type" name="claim_type">
			                        	<option value="0"> Select Claim Type</option>
				                        @foreach($claimTypes as $claimType)
				                          <option value="{{ $claimType->id }}"> {{ $claimType->type }}</option>
				                        @endforeach
			                    	</select>
			                    </div>
		                    </div>
							<div class="col-lg-2">
		                    	<div class="form-group">
			                     	<label for="claim_type">TAT:</label>
			                     	<select class="form-control input-sm" id="tat" name="tat">
			                        	<option value="0">Select TAT</option>
				                        @for($i = 0; $i < 10; $i++)
				                          <option value="{{ $i }}"> {{ $i }}</option>
				                        @endfor
			                    	</select>
			                    </div>
		                    </div>
							<div class="col-lg-1 margin-top-20">
								<button type="submit" class="btn btn-primary"> Filter </button>
							</div>
						</form>
					</div>
					<div class="col-lg-12">
						<table id="manage-case-table" class="table table-advance ">
							<thead>
								<tr>
									<th>Date</th>
									<th>Reference No</th>
									<th>Insurer</th>
									<th>Claim No.</th>
									<th>Insured</th>
									<th>Hospital</th>
									{{-- <th>Location</th> --}}
									<th>Assigned</th>
									<th>Status</th>
									<th>Amount</th>
									<th>TAT</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach($claims as $claim)
									<tr class="" data-id="{{ $claim->id }}" data-status="{{ $claim->status }}"  data-assigned="{{ $claim->assigned }}">
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->doi }}">{{ $claim->doi}}</td>
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->native_reference }}">{{ $claim->native_reference }}</td>
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->insurer_short_name }}">{{ $claim->insurer_short_name }}</td>
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->foreign_id }}">{{ $claim->foreign_id }}</td>
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->insurer_name }}">{{ $claim->insurer_name }}</td>
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->hospital_name }}">{{ $claim->hospital_name }}</td>
										{{-- <td data-container="body" data-placement="top" data-original-title="{{ $claim->location }}">{{ $claim->location }}</td> --}}
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->assigned_user }}">{{ $claim->assigned_user }}</td>
										<td data-container="body" data-placement="top" data-original-title="{{ $status[$claim->status] }}">{{ $status[$claim->status] }}</td>
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->claim_amount }}">{{ $claim->claim_amount }}</td>
										<td data-container="body" data-placement="top" data-original-title="{{ $claim->tat }}">{{ $claim->tat }}</td>
										<td>
											
											<!-- <button data-container="body" data-placement="top" data-original-title="Native Reference: {{ $claim->native_reference }}, <br>DOI: {{ $claim->doi }}, <br>Insurer Name: {{ $claim->insurer_name }}, <br>Hospital Name: {{ $claim->hospital_name }}, <br>Location: {{ $claim->location }}, <br>Insurer Short Name: {{ $claim->insurer_short_name }}, <br>Foreign ID: {{ $claim->foreign_id }}, <br>Assigned User: {{ $claim->assigned_user }}, <br>Status: {{ $status[$claim->status] }}, <br> Claim Amount: {{ $claim->claim_amount }}" class="btn btn-warning btn-xs tooltips" ><i class="fa fa-gavel"></i></button> -->
											<button data-container="body" data-placement="top" data-html="true" data-original-title="Update <br> Status" class="btn btn-warning btn-xs tooltips update-case-status" ><i class="fa fa-gavel"></i></button>
											<a data-container="body" data-placement="top" data-original-title="View" href="{{ url("/case/$claim->id/view") }} " class="btn btn-primary tooltips btn-xs"><i class="fa fa-eye"></i></a>
											@if(Session::get('user_role') != 3)
												<a data-container="body" data-placement="top" data-original-title="Edit" href="{{ url("/case/$claim->id/edit") }} " class="btn btn-primary tooltips btn-xs edit"><i class="fa fa-pencil"></i></a>
												<a data-container="body" data-placement="top" data-original-title="Delete" href=" {{ url("/case/$claim->id/delete") }} " class="delete-claim btn tooltips btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</section>
		</div>
	</div>
</section>

<!-- Start Modal -->



<div id="update-case-status-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
    	<form id="assign-case-to-field-officer-form" method="post" action=" {{ url('/case/assign-user/') }} " role="form" style="display: none;" >
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	      <input type="hidden" name="case_id" id="case_id">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Assign User</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
	        			<label for="select-user"> Select Field Officer </label>
	        			<select name="assign_user" id="assign_user" class="form-control" >
	        				@foreach ($users as $i => $user)
	        					<option value="{{ $user->id }}  "> {{ $user->first_name . ' ' . $user->last_name }} </option>
	        				@endforeach
	        			</select>
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
	        			<label for="select-user"> Message </label>
	        			<textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> ICP </label>
	        			<input type="file" id="" name="icp_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Report Entry </label>
	        			<input type="file" id="" name="report_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Bill & Receipt </label>
	        			<input type="file" id="" name="bill_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Others </label>
	        			<input type="file" id="" name="other_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Update</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      	</form>

      	<form id="end-fw-work-form" method="post" action=" {{ url('/case/fw-work-complete/') }} " role="form" style="display: none;" >
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	      <input type="hidden" name="case_id" id="case_id">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Field Work Completed</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
	        			<label for="claim_type"> Claim Type </label>
	        			<select id="fw_report" name="fw_report" class="form-control" >
	        				<option value="1"> Genuine  </option>
	        				<option value="0"> Not Genuine  </option>
		        		</select>
		        	</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
	        			<label for="select-user"> Message </label>
	        			<textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> ICP </label>
	        			<input type="file" id="" name="icp_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Report Entry </label>
	        			<input type="file" id="" name="report_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Bill & Receipt </label>
	        			<input type="file" id="" name="bill_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Others </label>
	        			<input type="file" id="" name="other_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Update</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      	</form>

      	<form id="create-case-report-form" method="post" action=" {{ url('/case/create-report/') }} " role="form" style="display: none;"  enctype="multipart/form-data">
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	      <input type="hidden" name="case_id" id="case_id">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Create Report</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
	        			<label for="select-user"> Message </label>
	        			<textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> ICP </label>
	        			<input type="file" id="" name="icp_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Report Entry </label>
	        			<input type="file" id="" name="report_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Bill & Receipt </label>
	        			<input type="file" id="" name="bill_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Others </label>
	        			<input type="file" id="" name="other_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Update</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      	</form>

      	<form id="end-case-form" method="post" action=" {{ url('/case/update-status/') }} " role="form" style="display: none;" >
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
	      <input type="hidden" name="case_id" id="case_id">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Udpate Staus</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
		        	<div class="form-group">
	        			<span class="form-control" id="update-status-text"></span>
	        			<select id="case_status" name="case_status" class="form-control hidden">
	        				<option value="4"> Mail Sent  </option>
	        				<option value="5"> Payment Done  </option>
	        				<option value="6"> Closed  </option>
		        		</select>
		        	</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
	        			<label for="select-user"> Message </label>
	        			<textarea class="form-control" id="message" name="message" placeholder="Message"></textarea>
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> ICP </label>
	        			<input type="file" id="" name="icp_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Report Entry </label>
	        			<input type="file" id="" name="report_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Bill & Receipt </label>
	        			<input type="file" id="" name="bill_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
	        			<label for="select-user"> Others </label>
	        			<input type="file" id="" name="other_files[]" class="form-control" multiple="multiple">
	        		</div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="submit" class="btn btn-primary">Update</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
      	</form>
    </div>
  </div>
</div>
<!-- End Modal -->
@stop

@section('scripts')
	
<script>
	var table_tools_link = "{{url('/js/data-tables/datatables/extensions/TableTools/')}}";
</script>
<script type="text/javascript" src="{{url('/js/data-tables/datatables/media/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{url('/js/data-tables/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{url('/js/data-tables/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>


      <script type="text/javascript" charset="utf-8">
          var EndPoint = {
          	"AssignFieldOfficer": "{{url('/case/assign-user/')}}",
			"EndFieldWork": "{{url('/case/fw-work-complete/')}}",
			"CreateReport": "{{url('/case/create-report/')}}",
			"FilterCase": "{{url('/case/all')}}",
			"UpdateStatus": "{{url('/case/update-status/')}}",
          }
          var Table;
        $(document).ready(function() {
			$('#nav-accordion #case-li a:eq(0)').click();
	    	$('#nav-accordion #case-li ul:eq(0) #manage-case-li').addClass("active").parents('.sub-menu').addClass("active");
            Case.init();
        	if ($("#manage-case-table tbody td").length > 5) {
				Table = $("#manage-case-table").DataTable({
				  	dom: 'T<"clear">lfrtip',
                  	"aaSorting": [[ 9, "desc" ]],
				});
			}
        });
      </script>
<script type="text/javascript">
	
</script>
<script type="text/javascript" language="javascript" src="{{url('/js/custom/manage-case.js')}}"></script>
@stop