@extends('master')

@section('title')
Case: Create
@stop

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{url('/js//bootstrap-datepicker/css/datepicker.css')}}"/>
@stop

@section('content')
<section class="wrapper ">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          Create Case
        </header>
        <div class="panel-body">
          @if(count($errors))
              <div class="row alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                @foreach($errors->all() as $error)
                    {{$error}}<br>
                @endforeach
              </div>
          @endif
          <div class="row">
            <form method="post" action="{{ url('/case/create') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="doi">Date of Intimation: <span class="required">*</span></label>
                        <input type="text" data-date="12-02-2012" viewMode=""  name="doi" id="doi" readonly="" size="16" class="form-control input-sm" value="{{ (old('doi'))? old('doi'): date('d-m-Y') }}" />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="insurer">Insurer <span class="required">*</span></label>
                      <select class="form-control input-sm" id="insurer" name="insurer">
                        <option value=""> Select Insurer</option>
                        @foreach($insurers as $insurer)
                          <option value="{{ $insurer->id }}"> {{ $insurer->name }} </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="claim_type">Claim Type: <span class="required">*</span></label>
                      <select class="form-control input-sm" name="claim_type" value="{{ old('claim_type') }}">
                        <option value=""> Select Claim Type</option>
                        @foreach($claimTypes as $claimType)
                          <option value="{{ $claimType->id }}"> {{ $claimType->type }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div id="clid-div" class="col-md-6" style="display: none;">
                    <div class="form-group">
                      <label for="clid"> CLID <span class="required">*</span></label>
                      <input type="text" class="form-control input-sm" name="clid" value="{{ old('clid') }}" placeholder="CLID *">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="claim_amount">Claim Amount: </label>
                      <input type="text" class="form-control input-sm" name="claim_amount" value="{{ old('claim_amount') }}" placeholder="Claim Amount">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="foreign_id">Claim No. <span class="required">*</span></label>
                      <input type="text" class="form-control input-sm" name="foreign_id" value="{{ old('foreign_id') }}" placeholder="Claim No.">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="policy_no">Policy No.: </label>
                      <input type="text" class="form-control input-sm" name="policy_no" value="{{ old('policy_no') }}" placeholder="Policy No.">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="native_reference">Our reference no. <span class="required">*</span></label>
                      <input type="text" class="form-control input-sm" name="native_reference" value="{{ old('native_reference') }}" placeholder="Our reference no.">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="insurer_name">Insured Name: </label>
                      <input type="text" class="form-control input-sm" name="insurer_name" value="{{ old('insurer_name') }}" placeholder="Insured Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="hospital_name">Hospital Name: </label>
                      <input type="text" class="form-control input-sm" name="hospital_name" value="{{ old('hospital_name') }}" placeholder="Hospital Name">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="location">Location: </label>
                      <textarea class="form-control" name="location" placeholder="Location">{{ old('location') }}</textarea>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="triggers">Triggers:</label>
                      <textarea class="form-control" name="triggers" placeholder="Triggers">{{ old('triggers') }}</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <div class="pull-right">
                      <input type="submit" class="btn btn-sm btn-primary" value="Save">
                      <a onclick="history.back();" class="btn btn-sm btn-danger"> Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
      </section>
    </div>
  </div>
</section>
@stop

@section('scripts')
<!-- <script type="text/javascript" src="{{url('/js/moment.min.js')}}"></script> -->
<script type="text/javascript" src="{{url('/js/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<script>
    $(function() {
      showHideClid();
      $('#nav-accordion #case-li a:eq(0)').click();
      $('#nav-accordion #case-li ul:eq(0) #create-case-li').addClass("active").parents('.sub-menu').addClass("active");
      $('#doi').datepicker({
        format: "dd-mm-yyyy",
        setValue: new Date(),
        setStartDate: new Date,
        showMeridian: true,
        autoclose: true,
        todayHighlight: true
        // todayBtn: true
      });
      // document.getElementById('doi').value = new Date().toISOString().substring(0, 10);
    });
    
    function showHideClid() {
      $('#insurer').off('change');
      $('#insurer').on('change', function(e) {
        e.preventDefault();
        $('#clid').val('');
        $('#clid-div').hide('slow');
        if ($(this).val() == '2') {
          $('#clid-div').show('slow');
        };
      });
    }
</script>
@stop