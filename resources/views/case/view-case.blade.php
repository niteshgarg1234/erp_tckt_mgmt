@extends('master')

@section('title')
Security
@stop

@section('styles')
  <style>
    .album {
      display: inline-block;
      width: 100%;
    }
    .album a img{
      margin: 2px;
    }
  </style>
@stop


@section('content')
<section class="wrapper ">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          View Case
        </header>
        <div class="panel-body">
          @if(count($errors))
              @foreach($errors->all() as $error)
              <div class="row alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{$error}}
              </div>
              @endforeach
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="">
                <!--timeline start-->
                <section class="panel">
                  <div class="panel-body">
                    <div class="text-center mbot30">
                      <h3 class="timeline-title">Timeline</h3>
                      <!-- <p class="t-info">This is a project timeline</p> -->
                    </div>
                    <div class="timeline">
                      @foreach($activities as $i => $activity)
                        <article class="timeline-item {{ ($i % 2 == 0 )? '': 'alt' }}">
                          <div class="timeline-desk">
                            <div class="panel">
                              <div class="panel-body">
                                <span class="arrow"></span>
                                <span class="timeline-icon blue"></span>
                                <span class="timeline-date"> {{ $activity->created_at }} </span>
                                <!-- <h1 class="blue">05 July | Monday</h1> -->
                                <?php $additional_parameter = json_decode($activity->additional_parameter); ?>
                                <div class="notification">
                                  <p> {!! $activity->message !!} </p>
                                  @if(isset($additional_parameter->message))
                                    <pre>{{ $additional_parameter->message }}</pre>
                                  @endif
                                </div>
                                  @if(isset($additional_parameter->icp_files) && count($additional_parameter->icp_files))
                                    <div class="album">
                                      <h4>ICP Files</h4>
                                      @foreach($additional_parameter->icp_files as $x => $file)
                                        <a href="{{ url($file->url) }}">{{ $file->name }}</a>
                                      @endforeach
                                    </div>
                                  @endif

                                  @if(isset($additional_parameter->report_files) && count($additional_parameter->report_files))
                                    <div class="album">
                                      <h4>Report Files</h4>
                                      @foreach($additional_parameter->report_files as $x => $file)
                                        <a href="{{ url($file->url) }}">
                                          <a href="{{ url($file->url) }}">{{ $file->name }}</a>
                                          {{-- <img alt="{{ $file->name }}" title="{{ $file->name }}" src="{{ url($file->preview) }}"> --}}
                                        </a>
                                      @endforeach
                                    </div>
                                  @endif

                                  @if(isset($additional_parameter->bill_files) && count($additional_parameter->bill_files))
                                    <div class="album">
                                      <h4>Bill & Receipt</h4>
                                      @foreach($additional_parameter->bill_files as $x => $file)
                                        <a href="{{ url($file->url) }}">
                                          <a href="{{ url($file->url) }}">{{ $file->name }}</a>
                                          {{-- <img alt="{{ $file->name }}" title="{{ $file->name }}" src="{{ url($file->preview) }}"> --}}
                                        </a>
                                      @endforeach
                                    </div>
                                  @endif

                                  @if(isset($additional_parameter->other_files) && count($additional_parameter->other_files))
                                    <div class="album">
                                      <h4>Other Files</h4>
                                      @foreach($additional_parameter->other_files as $x => $file)
                                        <a href="{{ url($file->url) }}">
                                          <a href="{{ url($file->url) }}">{{ $file->name }}</a>
                                          {{-- <img alt="{{ $file->name }}" title="{{ $file->name }}" src="{{ url($file->preview) }}"> --}}
                                        </a>
                                      @endforeach
                                    </div>
                                  @endif

                              </div>
                            </div>
                          </div>
                        </article>
                      @endforeach
                    </div>

                    <div class="clearfix">&nbsp;</div>
                  </div>
                </section>
                <!--timeline end-->
              </div>
              <div id="case-info-tab" class="hidden">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="doi">Date of Intimation: *</label>
                        <input type="date" class="form-control input-sm" name="doi" value="{{ $claim->doi }}" placeholder="Date of Intimation *">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="insurer">Insurer *</label>
                        <select class="form-control input-sm" name="insurer">
                          <option value=""> Select Insurer</option>
                          @foreach($insurers as $insurer)
                            <option value="{{ $insurer->id }}" @if($insurer->id == $claim->insurer) {{ 'selected' }} @endif> {{ $insurer->name }} </option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="claim_type">Claim Type: *</label>
                        <select class="form-control input-sm" name="claim_type" value="{{ $claim->claim_type }}">
                          <option value=""> Select Claim Type</option>
                          @foreach($claimTypes as $claimType)
                            <option value="{{ $claimType->id }}" @if($claimType->id == $claim->claim_type) {{ 'selected' }} @endif> {{ $claimType->type }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="clid"> CLID: *</label>
                        <input type="text" class="form-control input-sm" name="clid" value="{{ $claim->clid }}" placeholder="CLID *">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="claim_amount">Claim Amount: </label>
                        <input type="text" class="form-control input-sm" name="claim_amount" value="{{ $claim->claim_amount }}" placeholder="Claim Amount">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="foreign_id">Claim No. *</label>
                        <input type="text" class="form-control input-sm" name="foreign_id" value="{{ $claim->foreign_id }}" placeholder="Claim No.">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="policy_no">Policy No.: </label>
                        <input type="text" class="form-control input-sm" name="policy_no" value="{{ $claim->policy_no }}" placeholder="Policy No.">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="native_reference">Our reference no.</label>
                        <input type="text" class="form-control input-sm" name="native_reference" value="{{ $claim->native_reference }}" placeholder="Our reference no.">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="insurer_name">Insured Name: </label>
                        <input type="text" class="form-control input-sm" name="insurer_name" value="{{ $claim->insurer_name }}" placeholder="Insured Name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="hospital_name">Hospital Name: </label>
                        <input type="text" class="form-control input-sm" name="hospital_name" value="{{ $claim->hospital_name }}" placeholder="Hospital Name">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="location">Location: </label>
                        <textarea class="form-control" name="location" placeholder="Location">{{ $claim->location }}</textarea>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="triggers">Triggers:</label>
                        <textarea class="form-control" name="triggers" placeholder="Triggers">{{ $claim->triggers }}</textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    </div>
  </div>
</section>
@stop

@section('scripts')
<script>
    $(function() {
      $('#nav-accordion #case-li a:eq(0)').click();
      $('#nav-accordion #case-li ul:eq(0) #manage-case-li').addClass("active").parents('.sub-menu').addClass("active");
    });
</script>
@stop