@extends('master')

@section('title')
All Users
@stop

@section('content')
<section class="wrapper site-min-height">
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					All Users
					<a href="{{ url('/user/create') }} " class="btn btn-sm btn-primary pull-right tooltips" data-container="body" data-placement="top" data-original-title="Create New User">
						<!-- <i class="fa fa-plus-square"></i>  -->Create User
					</a>
				</header>
				<table class="table table-striped table-advance table-hover">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th><i class="fa fa-phone"></i> Contact no.</th>
							<th><i class="fa fa-calendar"></i> DOB</th>
							<th><i class="fa fa-calendar"></i> DOJ</th>
							<th class="hidden-phone"><i class="fa fa-question-circle"></i> Address</th>
							<th class="hidden-phone">User Type</th>
							<th>User Role</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
						<tr>
							<td>{{ $user->first_name . ' ' . $user->last_name }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->phone }}</td>
							<td>{{ $user->dob }}</td>
							<td>{{ $user->doj }}</td>
							<td class="hidden-phone">{{ $user->address }}</td>
							<td class="hidden-phone">{{ $user->type_name }}</td>
							<td>{{ $user->role_name }}</td>
							<td>
								<a href=" {{ url('/user/'.$user->id.'/edit') }} " class="btn btn-primary btn-xs edit tooltips" data-container="body" data-placement="top" data-original-title="Edit User"><i class="fa fa-pencil"></i></a>
								<a href="{{ url('/user/'.$user->id.'/delete') }}" class="delete-user btn btn-danger btn-xs tooltips" data-container="body" data-placement="top" data-original-title="Delete User"><i class="fa fa-trash-o "></i></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</section>
		</div>
	</div>
</section>
@stop

@section('scripts')
	<script type="text/javascript">
	$(function() {
		$('#nav-accordion #users-li a:eq(0)').click();
    	$('#nav-accordion #users-li ul:eq(0) #manage-users-li').addClass("active").parents('.sub-menu').addClass("active");
		deleteUser();		
	})

	function deleteUser() {
		$('.delete-user').off('click');
		$('.delete-user').on('click', function(e) {
			// var user = $(this).parents('tr').find('td:0').text();
			if (!confirm('Are you sure want to delete this')) {
				e.preventDefault();
			};
		});
	}
	</script>
@stop