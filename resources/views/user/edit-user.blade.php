@extends('master')

@section('title')
Security
@stop

@section('content')
<section class="wrapper site-min-height">
  <div class="row">
    <div class="col-lg-12">
      <section class="panel">
        <header class="panel-heading">
          My Profile
        </header>
        <div class="panel-body">
          @if(count($errors))
              @foreach($errors->all() as $error)
              <div class="row alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{$error}}
              </div>
              @endforeach
          @endif
          <div class="row">
            <form method="post" action="{{ url('/user/'.$user->id.'/edit') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="first_name">First Name:</label>
                      <input type="text" class="form-control input-sm" name="first_name" value="{{ $user->first_name }}" placeholder="First Name">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="last_name">Last Name:</label>
                      <input type="text" class="form-control input-sm" name="last_name" value="{{ $user->last_name }}" placeholder="Last Name">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Email ID:</label>
                      <input type="text" class="form-control input-sm" name="email" value="{{ $user->email }}" placeholder="Email ID">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Mobile:</label>
                      <input type="text" class="form-control input-sm" name="phone" value="{{ $user->phone }}" placeholder="Mobile">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="dob">Date of birth:</label>
                      <input type="date" class="form-control input-sm" name="dob" value="{{ $user->dob }}" placeholder="Date of birth">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="doj">Date of join:</label>
                      <input type="date" class="form-control input-sm" name="doj" value="{{ $user->doj }}" placeholder="Date of join">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="address">Address:</label>
                      <textarea class="form-control" name="address" placeholder="Address">{{ $user->address }}</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-12">
                    <div class="pull-right">
                      <input type="submit" class="btn btn-sm btn-primary" value="Update">
                      <a onclick="history.back();" class="btn btn-sm btn-danger"> Cancel</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
      </section>
    </div>
  </div>
</section>
@stop

@section('scripts')
<script>
    $(function() {
    });
</script>
@stop