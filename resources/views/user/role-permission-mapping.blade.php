@extends('master')

@section('title')
Create Role
@stop

@section('styles')
	<style type="text/css">
		.black-border  {
		    border: 1px solid;
		    padding: 10px;
	        margin: 5px;
			border-radius: 10px;
		    padding-bottom: 0px;
		}
		.nav.nav-pills.nav-justified li a {
		    border: 1px solid #928E8E;
		}
	</style>
@stop

@section('content')
<section class="wrapper site-min-height">
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					Roles and Permissions
				</header>
				<div class="panel-body">
					@if(count($errors))
					    @foreach($errors->all() as $error)
					    <div class="row alert alert-danger">
					        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					        {{$error}}
					    </div>
					    @endforeach
					@endif
					<div class="row">
						<div class="col-md-12 form-group">
							<div class="">
								<a href=" {{ url('/role/create') }} " class="btn btn-sm btn-primary pull-left"> Create Role</a>
								<a href=" {{ url('/permission/create') }} " class="btn btn-sm btn-primary pull-right"> Create Permission</a>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<ul class="nav nav-pills nav-justified">
									<li><a href="#roles-tab" data-toggle="tab">Roles</a></li>
									<li class="active"><a href="#mapping-tab" data-toggle="tab">Mapping</a></li>
									<li><a href="#permissions-tab" data-toggle="tab">Permissions</a></li>
								</ul>
							</div>
							<div class="tab-content form-group">
								<div id="roles-tab" class="tab-pane fade">
									<div class="col-md-12">
										<div class="table-scroable">
											<table class="table table-striped table-bordered">
												<thead>
												  <tr>
												    <th>S/N</th>
												    <th>Role</th>
												    <th>Created Time</th>
												    <th>Action</th>
												  </tr>
												</thead>
												<tbody>
													@foreach($roles as $i => $role)
													<tr>
													    <td> {{ ($i + 1) }} </td>
													    <td> {{ $role->role }} </td>
													    <td> {{ date('F d, Y', strtotime($role->created_at)) }} </td>
													    <td>
													   		 <a href="{{ url("/role/$role->id/edit") }} "><i class="fa fa-edit"></i></a>
													   		| <a href="{{ url("/role/$role->id/delete") }} "><i class="fa fa-trash"></i></a>
													    </td>
												 	</tr>
													@endforeach
												    
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div id="mapping-tab" class="tab-pane fade in active">
									<div class="col-md-12 black-border">
										<div class="col-md-3 black-border">
											@foreach($roles as $i => $role)
										   		<div class="form-group">
											   		<a class="btn btn-primary"> {{ $role->role }} </a>
												</div>	
											@endforeach
										</div>
										<div class="col-md-5 black-border">
											@foreach($permissions as $i => $permission)
									   			<div class="form-group">
													<a class="btn btn-primary"> {{ $permission->permission }} </a>
												</div>
											@endforeach
										</div>
										<div class="col-md-3 black-border">
											@foreach($permissions as $i => $permission)
									   			<div class="form-group">
													<a class="btn btn-primary"> {{ $permission->permission }} </a>
												</div>
											@endforeach
										</div>
									</div>
								</div>
								<div id="permissions-tab" class="tab-pane fade">
									<div class="col-md-12">
										<div class="table-scroable">
											<table class="table table-striped table-bordered">
												<thead>
												  <tr>
												    <th>S/N</th>
												    <th>Permission</th>
												    <th>Created Time</th>
												    <th>Action</th>
												  </tr>
												</thead>
												<tbody>
													@foreach($permissions as $i => $permission)
													<tr>
													    <td> {{ ($i + 1) }} </td>
													    <td> {{ $permission->permission }} </td>
													    <td> {{ date('F d, Y', strtotime($permission->created_at)) }} </td>
													    <td>
													   		 <a href="{{ url("/permission/$permission->id/edit") }} "><i class="fa fa-edit"></i></a>
													   		| <a href="{{ url("/permission/$permission->id/delete") }} "><i class="fa fa-trash"></i></a>
													    </td>
												 	</tr>
													@endforeach
												    
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
    $(function() {
    });
</script>
@stop