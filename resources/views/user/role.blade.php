@extends('master')

@section('title')
Create Role
@stop

@section('content')
<section class="wrapper site-min-height">
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					User Role
				</header>
				<div class="panel-body">
					@if(count($errors))
					    @foreach($errors->all() as $error)
					    <div class="row alert alert-danger">
					        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					        {{$error}}
					    </div>
					    @endforeach
					@endif
					<form role="form" action="{{ url('/role/create') }}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="role_name">Role Name</label>
								<input type="text" class="form-control" id="role_name" placeholder="New User Role" name="role_name" value="{{ old('role_name') }}">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>&nbsp;</label>
								<button type="submit" class="finish btn btn-danger btn-block">Create</button>
							</div>
						</div>
					</form>
				</div>
			</section>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
    $(function() {
    });
</script>
@stop