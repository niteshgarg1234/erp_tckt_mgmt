@extends('master')

@section('title')
Create User
@stop

@section('content')
<section class="wrapper site-min-height">
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					User Registartion
				</header>
				<div class="panel-body">
					@if(count($errors))
					    @foreach($errors->all() as $error)
					    <div class="row alert alert-danger">
					        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					        {{$error}}
					    </div>
					    @endforeach
					@endif
					<div class="stepy-tab">
						<ul id="default-titles" class="stepy-titles clearfix">
							<li id="default-title-0" class="current-step">
								<div>Step 1</div><span> </span>
							</li>
							<li id="default-title-1">
								<div>Step 2</div><span> </span>
							</li>
							<li id="default-title-2">
								<div>Step 3</div><span> </span>
							</li>
						</ul>
					</div>
					<form role="form" class="form-horizontal" id="default" action="{{ url('/user/save') }}" method="post">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<fieldset title="Step 1" class="step" id="default-step-0">
							<legend></legend>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="first_name">First Name</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" id="first_name" placeholder="Enter first name" name="first_name" value="{{ old('first_name') }}">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="last_name">Last Name</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" id="last_name" placeholder="Enter last name" name="last_name" value="{{ old('last_name') }}">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="email">Email Address</label>
									<div class="col-lg-8">
										<input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{ old('email') }}">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="phone">Contact Number</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" id="phone" placeholder="Enter Contact number" name="phone" value="{{ old('phone') }}">
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="col-lg-2 custom-label" for="address">Address</label>
									<div class="col-lg-10">
										<textarea class="form-control" id="address" placeholder="Enter your address" name="address">{{ old('address') }}</textarea>
									</div>
								</div>
							</div>
						</fieldset>
						<fieldset title="Step 2" class="step" id="default-step-1">
							<legend></legend>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="dob">Date Of Birth</label>
									<div class="col-lg-8">
										<input type="date" class="form-control" id="dob" placeholder="Enter DOB" name="dob" value="{{ old('dob') }}">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="doj">Date Of Joining</label>
									<div class="col-lg-8">
										<input type="date" class="form-control" id="doj" placeholder="Enter DOJ" name="doj" value="{{ old('doj') }}">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="role">User Role</label>
									<div class="col-lg-8">
										<select class="form-control" id="role" placeholder="Enter first name" name="role">
											@foreach($roles as $role)
											<option value="{{ $role->id }}">{{ $role->role }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="type">User Type</label>
									<div class="col-lg-8">
										<select class="form-control" id="type" placeholder="" name="type">
											@foreach($types as $type)
											<option value="{{ $type->id }}">{{ $type->type }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</fieldset>
						<fieldset title="Step 3" class="step" id="default-step-2">
							<legend> </legend>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="password">Password</label>
									<div class="col-lg-8">
										<input type="password" class="form-control" id="password" placeholder="Password" name="password" value="{{ old('password') }}">
									</div>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="col-lg-4 custom-label" for="confirm_password">Confirm Password</label>
									<div class="col-lg-8">
										<input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password" name="confirm_password" value="{{ old('confirm_password') }}">
									</div>
								</div>
							</div>
						</fieldset>
						<input type="submit" class="finish btn btn-danger" value="Finish"/>
					</form>
				</div>
			</section>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/jquery.stepy.js') }}"></script>
<script>
    $(function() {
        $('#nav-accordion #users-li a:eq(0)').click();
    	$('#nav-accordion #users-li ul:eq(0) #create-users-li').addClass("active").parents('.sub-menu').addClass("active");
		$('#default').stepy({
            backLabel: 'Previous',
            block: true,
            nextLabel: 'Next',
            titleClick: true,
            titleTarget: '.stepy-tab'
        });
    });
</script>
@stop