@extends('master')

@section('title')
Security
@stop

@section('content')
<section class="wrapper site-min-height">
	<div class="row">
		<div class="col-lg-12">
			<section class="panel">
				<header class="panel-heading">
					Change Password
				</header>
				<div class="panel-body">
					@if(count($errors))
					    @foreach($errors->all() as $error)
					    <div class="row alert alert-danger">
					        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					        {{$error}}
					    </div>
					    @endforeach
					@endif
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-8 col-md-offset-2">
								<form role="form" class="form-horizontal" id="update-password-form" action="{{ url('/user/change-password') }}" method="post">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-lg-4 custom-label" for="old_password">Current Password *</label>
											<div class="col-lg-8">
												<input type="password" class="form-control" id="old_password" placeholder="Current Password" name="old_password" value="{{ old('old_password') }}">
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-lg-4 custom-label" for="new_password">New Password *</label>
											<div class="col-lg-8">
												<input type="password" class="form-control" id="new_password" placeholder="New Password" name="new_password" value="{{ old('new_password') }}">
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label class="col-lg-4 custom-label" for="confirm_password">Confirm New Password *</label>
											<div class="col-lg-8">
												<input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password" name="confirm_password" value="{{ old('confirm_password') }}">
											</div>
										</div>
									</div>
									<input type="submit" class="finish btn btn-danger" value="Change Password"/>
								</form>
							</div>
						</div> 
					</div>
				</div>
			</section>
		</div>
	</div>
</section>
@stop

@section('scripts')
<script>
    $(function() {
        UpdatePassword.init($('#update-password-form'));
    });

    var UpdatePassword = {
    	updatePassword : function(form) {
    		$(form).off('submit');
    		$(form).on('submit', function(e) {
				if (!UpdatePassword.validations()) {
    				e.preventDefault();
					// $(form).submit();
				};
    		});
    	},

	    validations : function() {
	    	unsetError($('#old_password'));
	    	unsetError($('#new_password'));
	    	unsetError($('#confirm_password'));
	    	var old_password = $('#old_password').val();
	    	var new_password = $('#new_password').val();
	    	var confirm_password = $('#confirm_password').val();
	    	
	    	if (old_password.length < 4) {
	    		setError($('#old_password'), "Invalid Current password.");
	    		return false;
	    	};
	    	if (new_password.length < 4) {
	    		setError($('#new_password'), "Invalid new Password.");
	    		return false;
	    	};
	    	if (new_password != confirm_password) {
	    		setError($('#confirm_password'), "Password not matched.");
	    		return false;
	    	};
			return true;
	    },

	    init : function(form) {
	    	UpdatePassword.updatePassword(form);
	    }
    }
</script>
@stop