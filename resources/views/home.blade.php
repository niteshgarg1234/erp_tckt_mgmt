<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='http://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="{{ asset('/css/front_style.css') }}"> <!-- Resource style -->
	<link rel="stylesheet" href="{{ asset('/css/front_reset.css') }}"> <!-- CSS reset -->
	<script src="{{ asset('/js/modernizr.js') }}"></script> <!-- Modernizr -->
  	
	<title>Maccoy Consultancy Services - Home</title>
</head>
<body>
	<section class="cd-section">
		<div class="cd-block">
			<div class="navigation"><a href="{{ url('/login') }}">Login</a></div>
			<h1><span class="welcome-message">Welcome to</span> Maccoy Consultancy Services</h1>
		</div>
	</section> <!-- .cd-section -->

	<section class="cd-section">
		<div class="cd-block">
			<div class="cd-half-block"></div>

			<div class="cd-half-block">
				<h2>What do We Do?</h2>
				<p>Maccoy consultancy service is commited to building an incorporating work environment with passion and drive for excellence. Maccoy's collaborative work environment encourages open communication and frequent dialogue between our employees to provide it fundamental strength. Maccoy consultancy service proclaim you the best reasons why we should be hired.
				</p>
			</div>
		</div>
	</section> <!-- .cd-section -->

	<section class="cd-section">
		<div class="cd-block">
			<div class="cd-half-block"></div>

			<div class="cd-half-block">
				<div class="half-heading">Our Address</div>
				<ADDRESS>244, Sector 5<br>Gurgaon, New Delhi<br>Haryana - 579241</ADDRESS>
				<div class="half-heading">Contact At</div>
				<ADDRESS>Mr. Vishnu Sharma<br>+91-99654785213<br><a href="mailto:vishnu@maccoy.co.in">vishnu@maccoy.co.in</a></ADDRESS>
				<ADDRESS>Mr. Vishnu Sharma<br>+91-99654785213<br><a href="mailto:vishnu@maccoy.co.in">vishnu@maccoy.co.in</a></ADDRESS>
			</div>
		</div>
	</section> <!-- .cd-section -->

	<section class="cd-section">
		<div class="cd-block">
			<div class="cd-half-block"></div>

			<div class="cd-half-block">
				
					<div class="half-heading">Contact us</div>
					<form action="{{ url('/contact') }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="text" name="name" value="{{ old('name') }}" placeholder="Your Name" class="custom-form-control">
					<input type="text" name="email" value="{{ old('email') }}" placeholder="Your Email" class="custom-form-control">
					<textarea class="custom-form-control" name="message" placeholder="Your mesasge">{{ old('message') }}</textarea>
					<input type="submit" value="Contact Us" class="custom-form-control">
					</form>
				
				<div class="footer">
					Proudly Powered By: <a href="http://Cyberoze.com" target="_blank">CYberoze.com</a>
				</div>
			</div>
		</div>
	</section> <!-- .cd-section -->

	<nav>
		<ul class="cd-vertical-nav">
			<li><a href="#0" class="cd-prev inactive">Next</a></li>
			<li><a href="#0" class="cd-next">Prev</a></li>
		</ul>
	</nav> <!-- .cd-vertical-nav -->
<script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('js/front_main.js') }}"></script> <!-- Resource jQuery -->
</body>
</html>